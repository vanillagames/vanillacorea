﻿using UnityEngine;
using UnityEngine.UI;

namespace Vanilla {

#if UNITY_EDITOR
	using UnityEditor;
	[CustomEditor(typeof(ObjectAnchor))]
	public class ObjectAnchorEditor : VanillaEditor {

		public static GUIContent labelContent;
		public static GUIStyle labelStyle = new GUIStyle();
		ObjectAnchor anchor;

		public void OnEnable() {
			ObjectAnchor anchor = (ObjectAnchor)target;

			if (anchor == null) {
				return;
			}

			labelStyle.alignment = TextAnchor.MiddleCenter;

			labelContent = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/+Vanilla/Vanilla/Textures/UI/Chiclet256.png", typeof(Texture)), "This is the anchors key string, and can be used to easily find this position in world space.");

			labelContent.text = "ObjectAnchor\n[" + anchor.anchorName + "]";
		}

		public void OnSceneGUI() {

			if (anchor == null) {
				return;
			}



			//Handles.Label(anchor.transform.position + Vector3.up * 2, "ObjectAnchor\n[" + anchor.anchorName + "]");



			Handles.Label(anchor.transform.position + Vector3.up - Vector3.right, labelContent, labelStyle);


			//Handles.Label(anchor.transform.position + Vector3.up * 2, anchor.transform.position.ToString() + "\nAnchor [" + anchor.anchorName + "]");

			//Handles.BeginGUI();

			//if (GUILayout.Button("Reset Anchor", GUILayout.Width(100))) {
			//	anchor.transform.SetPositionAndRotation(Vector3.zero, new Quaternion(0, 0, 0, 0));
			//}

			//Handles.EndGUI();


			//Handles.DrawWireArc(anchor.transform.position, anchor.transform.up, -anchor.transform.right, 180, anchor.value);

			//anchor.value = Handles.ScaleValueHandle(anchor.value, anchor.transform.position + anchor.transform.forward * anchor.value, anchor.transform.rotation,
			//		1, Handles.ConeHandleCap, 1);
		}
	}
#endif
}