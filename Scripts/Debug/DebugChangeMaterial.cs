﻿using UnityEngine;

using System;

using Vanilla.Debugging;

public class DebugChangeMaterial : VanillaDebugBase {

	MeshRenderer meshRenderer;

	public Material[] materials;

	public override void Awake() {
		base.Awake();

		meshRenderer = GetComponent<MeshRenderer>();
	}

	public void UseMaterial(int materialID) {
		meshRenderer.material = materials[materialID];
	}
}