﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Inputs;

namespace Vanilla.Debugging {
	public class DebugCamController : VanillaCamera {

		[Header("Moving")]
		public float defaultMoveSpeed = 0.1f;
		public float fastMoveSpeed = 1.0f;

		[Header("Looking")]
		public float ySensitivity = 8.0f;
		public float xSensitivity = 8.0f;

		public float smoothing = 16.0f;

		Quaternion targetRotation;
		Vector3 targetPosition;

		[ReadOnly]
		public float inputX;

		[ReadOnly]
		public float inputY;

		[ReadOnly]
		public bool turboMode;

		[Header("References")]
		public GameObject spawnablePrefab;

		public List<GameObject> spawnables;

		//[Header("Input")]
		//public InputEventSubscription toggleCameraSubscription;
		//public InputEventSubscription toggleControlSubscription;

		//public InputEventSubscription pitchSubscription;
		//public InputEventSubscription yawSubscription;

		//public InputEventSubscription forwardSubscription;
		//public InputEventSubscription strafeSubscription;

		//public InputEventSubscription activateTurboSubscription;
		//public InputEventSubscription deactivateTurboSubscription;

		//public InputEventSubscription spawnableSubscription;
		//public InputEventSubscription spawnableCleanupSubscription;

		public override void Initialize() {
			base.Initialize();
		}

		public override void PostInitialize() {
			//toggleCameraSubscription.Subscribe(ToggleCamera);
			//toggleControlSubscription.Subscribe(ToggleControl);
		}

		public override void OnEnable() {
			base.OnEnable();

			targetPosition = transform.position;

			//pitchSubscription.Subscribe(PitchInput);
			//yawSubscription.Subscribe(YawInput);

			//forwardSubscription.Subscribe(ForwardInput);
			//strafeSubscription.Subscribe(StrafeInput);

			//activateTurboSubscription.Subscribe(TurnOnTurboMode);
			//deactivateTurboSubscription.Subscribe(TurnOffTurboMode);

			//spawnableSubscription.Subscribe(HandleSpawnable);
			//spawnableCleanupSubscription.Subscribe(HandleSpawnableCleanup);
		}

		public override void OnDisable() {
			base.OnDisable();

			//pitchSubscription.Unsubscribe(PitchInput);
			//yawSubscription.Unsubscribe(YawInput);

			//forwardSubscription.Unsubscribe(ForwardInput);
			//strafeSubscription.Unsubscribe(StrafeInput);

			//activateTurboSubscription.Unsubscribe(TurnOnTurboMode);
			//deactivateTurboSubscription.Unsubscribe(TurnOffTurboMode);

			//spawnableSubscription.Unsubscribe(HandleSpawnable);
			//spawnableCleanupSubscription.Unsubscribe(HandleSpawnableCleanup);
		}

		public void ToggleCamera() {
			//Log("Hi");
			cam.enabled = !cam.enabled;
		}

		public void ToggleControl() {
			//Log("Hi 2");
			enabled = !enabled;
		}

		public void TurnOnTurboMode() {
			turboMode = true;
		}

		public void TurnOffTurboMode() {
			turboMode = false;
		}

		public void ForwardInput(float input) {
			targetPosition += transform.forward * -input * (turboMode ? fastMoveSpeed : defaultMoveSpeed);
		}

		public void StrafeInput(float input) {
			targetPosition += transform.right * input * (turboMode ? fastMoveSpeed : defaultMoveSpeed);
		}

		public void PitchInput(float input) {
			Vector3 newEulers = targetRotation.eulerAngles + new Vector3(-input * ySensitivity, 0, 0);

			newEulers.z = 0;

			targetRotation = Quaternion.Euler(newEulers);
		}

		public void YawInput(float input) {
			Vector3 newEulers = targetRotation.eulerAngles + new Vector3(0, input * xSensitivity, 0);

			newEulers.z = 0;

			targetRotation = Quaternion.Euler(newEulers);
		}

		public void HandleSpawnable() {
			spawnables.Add(Instantiate(spawnablePrefab, transform.forward, Quaternion.identity, null));
		}

		public void HandleSpawnableCleanup() {
			for (int i = spawnables.Count-1; i > 0; i--) {
				Destroy(spawnables[i]);
			}

			spawnables.Clear();
		}

		void Update() {
			//targetRotation += new Vector3(-inputY * ySensitivity, inputX * xSensitivity, 0);


			//float currentSpeed = turboMode ? fastMoveSpeed : defaultMoveSpeed;

			//if (Input.GetKey(KeyCode.W)) {
			//	targetPosition += transform.forward * currentSpeed;
			//}

			//if (Input.GetKey(KeyCode.S)) {
			//	targetPosition -= transform.forward * currentSpeed;
			//}

			//if (Input.GetKey(KeyCode.D)) {
			//	targetPosition += transform.right * currentSpeed;
			//}

			//if (Input.GetKey(KeyCode.A)) {
			//	targetPosition -= transform.right * currentSpeed;
			//}

			transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * smoothing);

			transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smoothing);
		}
	}
}