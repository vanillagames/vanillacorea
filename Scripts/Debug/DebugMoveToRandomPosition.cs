﻿using UnityEngine;

namespace Vanilla.Debugging {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(DebugMoveToRandomPosition))]
	public class DebugMoveToRandomPositionEditor : Editor {

		public DebugMoveToRandomPosition script;

		void OnEnable() {
			script = (DebugMoveToRandomPosition)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (GUILayout.Button("Move")) {
				script.Move();
			}
		}
	}
#endif

	public class DebugMoveToRandomPosition : VanillaBehaviour {

		[Header("[ Debug Move To Random Position ]")]
		public Vector2 xRange;
		public Vector2 yRange;
		public Vector2 zRange;

		public bool snapXToGrid;
		public bool snapYToGrid;
		public bool snapZToGrid;

		public float gridSizeX = 0.5f;
		public float gridSizeY = 0.5f;
		public float gridSizeZ = 0.5f;

		public bool localMode;

		public void Move() {
			Vector3 newPosition = new Vector3(Random.Range(xRange.x, xRange.y), Random.Range(yRange.x, yRange.y), Random.Range(zRange.x, zRange.y));

			if (snapXToGrid) {
				if (gridSizeX <= 0) {
					Error("GridSize X can't be less than 0.");
					return;
				}

				newPosition.x = Mathf.Round(newPosition.x / gridSizeX) * gridSizeX;
			}

			if (snapYToGrid) {
				if (gridSizeY <= 0) {
					Error("GridSize Y can't be less than 0.");
					return;
				}

				newPosition.y = Mathf.Round(newPosition.y / gridSizeY) * gridSizeY;
			}

			if (snapZToGrid) {
				if (gridSizeZ <= 0) {
					Error("GridSize Z can't be less than 0.");
					return;
				}

				newPosition.z = Mathf.Round(newPosition.z / gridSizeZ) * gridSizeZ;
			}

			if (localMode) {
				transform.localPosition = newPosition;
			} else {
				transform.position = newPosition;
			}
		}
	}
}