﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Events;

using System.Collections.Generic;

using UnityEditor;
using UnityEditor.SceneManagement;

namespace Vanilla.Debugging {
	[CustomEditor(typeof(DebugInstantiate))]
	public class DebugInstantiaterEditor : Editor {

		public DebugInstantiate script;

		void OnEnable() {
			script = (DebugInstantiate)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (GUILayout.Button("Create")) {
				script.Create();
			}

			if (GUILayout.Button("Clean Up")) {
				script.Cleanup();
			}
		}
	}

	public class DebugInstantiate : VanillaBehaviour {

		public UnityEvent preCreate;
		public UnityEvent postCreate;

		[Header("[ Debug Instantiate ]")]
		public bool parentToThisOnCreation;

		public GameObject[] prefabs;

		[ReadOnly]
		public List<GameObject> createdObjects;

		public void Create() {
			preCreate.Invoke();

			int prefabIndex = Random.Range(0, prefabs.Length - 1);

			GameObject newGO = PrefabUtility.InstantiatePrefab(prefabs[prefabIndex]) as GameObject;

			createdObjects.Add(newGO);

			newGO.transform.position = transform.position;
			newGO.transform.rotation = transform.rotation;

			EditorSceneManager.MoveGameObjectToScene(newGO, gameObject.scene);

			postCreate.Invoke();

			if (parentToThisOnCreation) {
				newGO.transform.SetParent(transform);
			}
		}

		public void Cleanup() {
			for (int i = createdObjects.Count - 1; i > -1; i--) {
				if (createdObjects[i] != null) {
					DestroyImmediate(createdObjects[i].gameObject);
				}
			}

			createdObjects.Clear();
		}
	}
}
#endif