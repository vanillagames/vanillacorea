﻿using UnityEngine;

using Vanilla.Inputs;

namespace Vanilla.Debugging {
	public class DebugResetPosition : VanillaDebugBase {

		// These need to be agnostic since its project in-specific
		public ProjectAction setPosition;
		public ProjectAction resetToPosition;

		public bool setOnAwake;

		Vector3 origPosition;
		Quaternion origRotation;

		Rigidbody rb;

		public override void Awake() {
			base.Awake();

			rb = GetComponent<Rigidbody>();

			if (VanillaInputs.i) {
				VanillaInputs.i.Subscribe(setPosition, ResetPosition);
				VanillaInputs.i.Subscribe(resetToPosition, SetResetPosition);
			}

			if (setOnAwake) {
				SetResetPosition();
			}
		}

		public void SetResetPosition() {
			origPosition = transform.position;
			origRotation = transform.rotation;
		}

		public void ResetPosition() {
			transform.position = origPosition;
			transform.rotation = origRotation;

			if (rb) {
				rb.velocity = Vector3.zero;
				rb.angularVelocity = Vector3.zero;
			}
		}
	}
}