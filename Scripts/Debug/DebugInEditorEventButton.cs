﻿using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Debugging {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(DebugInEditorEventButton))]
	public class DebugInEditorEventButtonEditor : Editor {

		public DebugInEditorEventButton script;

		void OnEnable() {
			script = (DebugInEditorEventButton)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (!Application.isPlaying) {
				if (GUILayout.Button("Fire Event")) {
					script.onEditorButtonClick.Invoke();
				}

				if (GUILayout.Button("Fire Event Repeatedly")) {
					for (int i = 0; i < script.repeatCount; i++) {
						script.onEditorButtonClick.Invoke();
					}
				}
			}
		}
	}
#endif

	/// <summary>
	/// 
	/// </summary>
	public class DebugInEditorEventButton : VanillaBehaviour {

		[Header("[ In Editor Button ]")]
		public int repeatCount;

		public UnityEvent onEditorButtonClick;
	}
}