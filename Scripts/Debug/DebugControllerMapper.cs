﻿using UnityEngine;

namespace Vanilla.Debugging {
	public class DebugControllerMapper : VanillaDebugBase {

		public int buttonTestLimit = 17;
		public int axisTestLimit = 28;

		public bool ignoreAxisValuesOf0 = true;

		public void Update() {
			for (int i = 0; i < buttonTestLimit; i++) {
				try {
					Input.GetButtonDown(i.ToString());
				} catch {
					Error("This button doesn't exist apparently [{0}]", i);
					return;
				}

				if (Input.GetButtonDown(i.ToString())) {
					Log("Raw Button Down [{0}]", i);
				}

				if (Input.GetButton(i.ToString())) {
					Log("Raw Button [{0}]", i);
				}

				if (Input.GetButtonUp(i.ToString())) {
					Log("Raw Button Up [{0}]", i);
				}
			}

			for (int i = 0; i < axisTestLimit; i++) {
				float a = 0;

				try {
					a = Input.GetAxis(i.ToString());

					//if (ignoreAxisValuesOf0 && a == 0) {
					//	return;
					//}

					Log("Raw Axis [{0}] - [{1}]", i, a);
				}
				catch {
					Error("This axis doesn't exist apparently [{0}]", i);
				}
			}
		}
	}
}