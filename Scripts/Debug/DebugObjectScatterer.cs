﻿using UnityEngine;

namespace Vanilla.Debugging {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(DebugObjectScatterer))]
	public class NewVanillaBehaviourEditor : Editor {

		public DebugObjectScatterer script;

		void OnEnable() {
			script = (DebugObjectScatterer)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (GUILayout.Button("Scatter Children")) {
				script.Scatter();
			}
		}
	}
#endif

	public class DebugObjectScatterer : VanillaBehaviour {

		[Header("[ Debug Object Scatterer ]")]
		public int xRange;
		public int yRange;
		public int zRange;

		public bool instantiateMode;

		public GameObject[] prefabs;

		[Tooltip("If true, the children will be scattered in the local space of this object.")]
		public bool localScatter;

		public bool randomizeRotationAsWell;

		public void Scatter() {
			for (int i = 0; i < transform.childCount; i++) {
				Vector3 newPosition = new Vector3(Random.Range(-xRange, xRange), Random.Range(-yRange, yRange), Random.Range(-zRange, zRange));

				if (localScatter) {
					transform.GetChild(i).localPosition = newPosition;
				} else {
					transform.GetChild(i).position = newPosition;
				}

				if (randomizeRotationAsWell) {
					transform.GetChild(i).rotation = Random.rotation;
				}
			}
		}
	}
}