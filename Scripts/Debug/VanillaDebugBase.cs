﻿using UnityEngine;

using System;

namespace Vanilla.Debugging {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(VanillaDebugBase))]
	public class VanillaDebugBaseEditor : Editor {

		public VanillaDebugBase script;

		void OnEnable() {
			script = (VanillaDebugBase)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (GUILayout.Button("Remove all instances of this script")) {
				VanillaDebugBase[] instances = FindObjectsOfType<VanillaDebugBase>();

				for (int i = 0; i < instances.Length; i++) {
					Destroy(instances[i]);
				}
			}
		}
	}
#endif

	public class VanillaDebugBase : VanillaBehaviour {


		public virtual void Awake() {
#if !VanillaDebug
			Error("I'm a debug script that is present in a non-debug build. I'm outta here!");
			Destroy(this);
#endif
		}

		public virtual void OnEnable() {
#if !VanillaDebug
			return;
#endif
		}
	}
}