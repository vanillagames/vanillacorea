﻿using UnityEngine;

using System;
using System.Diagnostics;

using System.Collections.Generic;

namespace Vanilla.Debugging {
	public class DebugFunctionStopwatch : MonoBehaviour {
//#if UNITY_EDITOR

		[Header("[ Function Stop Watch ]")]
		// The stopwatch we use. I don't think it needs to be reborn at the start of every test, but Reset() didn't... reset it.
		Stopwatch watch;

		// The TimeSpan we add each frame to. Eventually divided by frameMax and used to spit out the milliseconds.
		TimeSpan timeCalc;

		[Tooltip("How many frames should we average the function over?")]
		public int frameMax = 120;

		[Tooltip("The average time taken to complete the function.")]
		public float averageMilliseconds = 0;

		[Tooltip("The longest average time taken to complete the function.")]
		public float peakMilliseconds = 0;


		// A copy of each stopwatch recording result.
		List<TimeSpan> frameResult = new List<TimeSpan>();

		// Kind of like custom UnityEvents, this is simply declaring a type of function.
		// So for example, if you wanted to pass a float function, you'd need a float kind of delegate instead this void one.
		// When functions from elsewhere get passed in, they just simply have to match the delegate declaration. Pretty simple!
		public delegate void TimerTest();
//#endif

		/// <summary>
		/// This records how long a given function takes to complete in milliseconds.
		/// </summary>
		/// <param name="functionToTest">The function you want to record the milliseconds of.</param>
		public void RecordFunctionRunTime(TimerTest functionToTest) {
//#if UNITY_EDITOR
			watch = new Stopwatch();

			watch.Start();

			functionToTest();

			watch.Stop();

			frameResult.Add(watch.Elapsed);

			if (frameResult.Count > frameMax) {
				frameResult.RemoveAt(0);
			}

			timeCalc = TimeSpan.Zero;

			for (int i = 0; i < frameResult.Count; i++) {
				timeCalc += frameResult[i];
			}

			averageMilliseconds = timeCalc.Milliseconds / frameMax;

			if (averageMilliseconds > peakMilliseconds) {
				peakMilliseconds = averageMilliseconds;
			}
//#endif
		}
	}
}