﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

namespace Vanilla {
	[CreateAssetMenu(fileName = "Vanilla/Scene Asset", menuName = "Vanilla/Scene Asset")]
	[System.Serializable]
	public class VanillaSceneAsset : ScriptableObject {

		[Header("Scene Asset")]
		[Tooltip("Is this scene allowed to be loaded more than once?")]
		[SerializeField]
		public bool allowMultipleInstances;

		//[Tooltip("This phrase will be used for finding the scene in the sceneDictionary. If its blank, the scenes name will be used.")]
		//[SerializeField]
		//public string sceneNickname;

		//[Tooltip("This phrase can be used to describe the role of the scene. At some point, the SceneLoader may use this phrase as part of its loading process to inform the user.")]
		//[SerializeField]
		//public string loadingDescription;

		[Tooltip("The only way to store a reference to a Unity Scene file is by a generic object field. As a result, this field can be populated by any type of asset. To prevent headaches, please ensure the file is a Scene file.")]
		[SerializeField]
		public Object scene;

		//[ReadOnly]
		//public string loadString;

		[ReadOnly]
		[SerializeField]
		public int buildID;

		//[HideInInspector]
		[ReadOnly]
		public VanillaScene loadedScene;

		[Header("Magic auto-optimized don't-run-that-coroutine flags")]
		[SerializeField]
		[ReadOnly]
		public bool sceneLoadedIsOverridden;
		[SerializeField]
		[ReadOnly]
		public bool sceneUnloadIsOverridden;
		[SerializeField]
		[ReadOnly]
		public bool setupLoadedIsOverridden;
		[SerializeField]
		[ReadOnly]
		public bool setupUnloadedIsOverridden;
		[SerializeField]
		[ReadOnly]
		public bool activateIsOverridden;
		[SerializeField]
		[ReadOnly]
		public bool deactivateIsOverridden;

		//public string GetNameForDictionary() {
		//	if (string.IsNullOrEmpty(sceneNickname)) {
		//		return name;
		//	} else {
		//		return sceneNickname;
		//	}
		//}
	}
}