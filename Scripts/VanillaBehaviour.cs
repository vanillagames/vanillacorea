﻿using UnityEngine;

using System;
using System.IO;

using Vanilla.VanillaText;

namespace Vanilla {
	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// VanillaEditor
	///--------------------------------------------------------------------------------------------------------------------------------------------------
	
	#if UNITY_EDITOR
	using UnityEditor;

	[CanEditMultipleObjects]
	[CustomEditor(typeof(VanillaBehaviour), true)]
	public class VanillaEditor : CustomEditorBase {
		[Header("Editor settings")]
		[Tooltip("This name will be looked for within Assets/+Vanilla/Vanilla/Fonts")]
		public static string fontFileName = "modernsanslight.otf";

		[Tooltip("This name will be looked for within Assets/+Vanilla/Vanilla/Textures")]
		public static string logoFileName = "Vanilla Logo.png";

		public static bool editorContentInitialized;
		public static GUIContent logoContent;

		public static Font headingFont;
		public static GUIStyle headingStyle;

		//public static string[] propertiesToExclude;

		public VanillaBehaviour behaviour;
		string constructedBehaviourName;

		SerializedProperty localDebugEnum;

		public void Awake() {
			if (!editorContentInitialized) {
				headingFont = (Font)AssetDatabase.LoadAssetAtPath("Assets/+Vanilla/Vanilla/Fonts/" + fontFileName, typeof(Font));
				logoContent = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/+Vanilla/Vanilla/Textures/" + logoFileName, typeof(Texture)), "It's all about perspective.");

				headingStyle = new GUIStyle();
				headingStyle.fontSize = 24;
				headingStyle.fontStyle = FontStyle.Normal;
				headingStyle.font = headingFont;

				editorContentInitialized = true;
			}

			behaviour = (VanillaBehaviour)target;
			constructedBehaviourName = VanillaText.VanillaText.Spacify(behaviour.GetType().Name);
		}

		public override void OnInspectorGUI() {
			DrawVanillaHeader();

			base.OnInspectorGUI();
		}

		public void DrawVanillaHeader() {
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.HelpBox(VanillaCoreEditor.logoContent, true);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			EditorGUILayout.Space();
			EditorGUILayout.Space();

			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(constructedBehaviourName, VanillaCoreEditor.headingStyle);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}

		public void DrawAssetCreationButton(VanillaModule assetCreationLocation, Type assetType) {
			assetCreationLocation = (VanillaModule)EditorGUILayout.EnumPopup("Asset Creation Location", assetCreationLocation);

			if (!Application.isPlaying && GUILayout.Button("Create as new asset file")) {
				string assetPath = GetModulePath(assetCreationLocation);

				if (File.Exists(assetPath)) {
					VanillaLog.Log("There is already an asset by this name at the following path [{0}]", assetPath);
					Selection.activeObject = AssetDatabase.LoadAssetAtPath(assetPath, assetType);
					return;
				}

				VanillaSceneAsset newSceneAsset = CreateInstance<VanillaSceneAsset>();

				AssetDatabase.CreateAsset(newSceneAsset, assetPath);

				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
				EditorUtility.FocusProjectWindow();
				Selection.activeObject = newSceneAsset;
			}
		}

		public string GetModulePath(VanillaModule moduleType) {
			string path = string.Empty;

			path = "Assets";

			switch (moduleType) {
				case VanillaModule.Project:
					path += "/+Project";
					break;
				default:
					path += "/+Vanilla/" + moduleType.ToString();
					break;
			}

			return path;
		}
	}
	#endif

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// VanillaBehaviour
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	public class VanillaBehaviour : MonoBehaviour {

		[Header("[ Vanilla Behaviour ]")]
		//[EnumFlags(3)]
		//public DebugMode localDebugMode;

		[EnumFlags(3)]
		[SerializeField]
		public DebugFlags debugFlags;

		public void Log(object message) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Logs)) {
				VanillaLog.Log(gameObject, message);
			}
#endif
		}

		public void Log(string message, params object[] parameters) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Logs)) {
				VanillaLog.Log(gameObject, message, parameters);
			}
#endif
		}

		public void Warning(object message) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Warnings)) {
				VanillaLog.Warning(gameObject, message);
			}
#endif
		}

		public void Warning(string message, params object[] parameters) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Warnings)) {
				VanillaLog.Warning(gameObject, message, parameters);
			}
#endif
		}

		public void Error(object message) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Errors)) {
				VanillaLog.Error(gameObject, message);
			}
#endif
		}

		public void Error(string message, params object[] parameters) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Errors)) {
				VanillaLog.Error(gameObject, message, parameters);
			}
#endif
		}

		public void DebugRay(Vector3 origin, Vector3 direction, Color color, float duration = 0.1f) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Rays)) {
				Debug.DrawRay(origin, direction, color, duration);
			}
#endif
		}

		public void DebugLine(Vector3 origin, Vector3 destination, Color color, float duration = 0.1f) {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Rays)) {
				Debug.DrawLine(origin, destination, color, duration);
			}
#endif
		}

		public void Break() {
#if VanillaDebug
			if (debugFlags.HasFlag(DebugFlags.Breaks)) {
				Debug.Break();
			}
#endif
		}

		public bool CanDrawGizmos() {
#if VanillaDebug
			return debugFlags.HasFlag(DebugFlags.Gizmos);
#else
			return false;
#endif
		}

		public bool CanDebugMisc1() {
#if VanillaDebug
			return debugFlags.HasFlag(DebugFlags.Misc1);
#else
			return false;
#endif
		}

		public bool CanDebugMisc2() {
#if VanillaDebug
			return debugFlags.HasFlag(DebugFlags.Misc2);
#else
			return false;
#endif
		}

		public bool CanDebugMisc3() {
#if VanillaDebug
			return debugFlags.HasFlag(DebugFlags.Misc3);
#else
			return false;
#endif
		}
	}
}