﻿//using UnityEngine;
//using UnityEngine.UI;

//using System.Collections.Generic;

//namespace Vanilla.EventSystems {
//	public class PointerTracker : VanillaBehaviour {

//		[Header("EventSystem Raycasting")]
//		public float raycastInterval;
//		float rayTimer;
//		int currentRayToFire;

//		private static List<BasePointerOLD> _pointers;
//		public static List<BasePointerOLD> pointers {
//			get {
//				if (_pointers == null) {
//					_pointers = new List<BasePointerOLD>();
//				}

//				return _pointers;
//			}
//			set {
//				_pointers = value;
//			}
//		}

//		public Text readout;

//		public void Update() {
//			for (int i = 0; i < pointers.Count; i++) {
//				pointers[i].FireRay();
//			}

//#if VanillaDebug
//			UpdateReadout();
//#endif
//		}

//		public void UpdateOld() {
//			rayTimer += Time.deltaTime;

//			if (rayTimer > raycastInterval) {
//				currentRayToFire++;

//				if (currentRayToFire > pointers.Count) {
//					currentRayToFire = 1;
//					Log("Cycle complete!____________________________________________________________");
//				}

//				pointers[currentRayToFire-1].FireRay();

//				rayTimer -= raycastInterval;

//#if VanillaDebug
//				UpdateReadout();
//#endif
//			}
//		}

//		/// <summary>
//		/// When we enable or disable a pointer, it adds itself to the global list of pointers.\n\nBecause we don't know the order that they'll be enabled or disabled, this is our way of tracking them internally.
//		/// </summary>
//		public static void UpdatePointerIDs() {
//			for (int i = 0; i < pointers.Count; i++) {
//				pointers[i].pointerID = pointers.IndexOf(pointers[i]);
//			}
//		}

//		/// <summary>
//		/// Returns whether a Pointers localHit object is already 'seen' or 'entered' by another Pointer. This is useful for knowing when or when not to send Enter/Exit events.
//		/// </summary>
//		/// <param name="pointerID"></param>
//		/// <returns></returns>
//		public static bool LocalHitIsNotEnteredByAnotherPointer(int pointerID) {
//			for (int i = 0; i < pointers.Count; i++) {
//				// We also want to ensure that we're not checking what this pointer has stored, we only want to check the others.
//				if (i != pointerID && pointers[pointerID].currentHit == pointers[i].currentHit) {
//					return false;
//				}
//			}

//			return true;
//		}

//		public void UpdateReadout() {
//			if (pointers.Count < 1) {
//				return;
//			}

//			readout.text = string.Empty;

//			for (int i = 0; i < pointers.Count-1; i++) {
//				if (pointers[i].currentHit) {
//					readout.text += "Pointer[ " + i + " ] - [ " + pointers[i].currentHit + " ]\n";
//				} else {
//					readout.text += "Pointer[ " + i + " ] - [ Empty ]\n";
//				}
//			}

//			int lastValue = pointers.Count - 1;

//			if (pointers[lastValue].currentHit) {
//				readout.text += "Pointer[ " + lastValue + " ] - [ " + pointers[lastValue].currentHit + " ]";
//			} else {
//				readout.text += "Pointer[ " + lastValue + " ] - [ Empty ]";
//			}
//		}
//	}
//}