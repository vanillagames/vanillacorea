﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla {
	[CreateAssetMenu(fileName = "Vanilla/New Scene Setup", menuName = "Vanilla/Scene Setup")]
	public class SceneSetup : ScriptableObject {

		[SerializeField]
		public int loadingScreenID = -1;

		//[Tooltip("The active scene is the scene that new GameObjects, if any, will be created in.")]
		//[SerializeField]
		//public int sceneToActivateOnLoadCompletion;
		//public VanillaSceneAsset sceneToActivate;

		[Header("Scenes")]
		public VanillaSceneAsset[] sceneAssets;

		//public Scene GetSceneToActivate() {
		//	if (sceneToActivateOnLoadCompletion > sceneAssets.Length) {
		//		VanillaLog.Warning("The scene to activate for setup [{0}] is out of index range. As a result, we'll just activate the first scene instead.");
		//		return SceneManager.GetSceneByName(sceneAssets[0].name);
		//	}

		//	return SceneManager.GetSceneByName(sceneAssets[sceneToActivateOnLoadCompletion].name);
		//}

		//public string GetActiveSceneName() {
		//	if (sceneToActivateOnLoadCompletion > sceneAssets.Length) {
		//		VanillaLog.Warning("The scene to activate for setup [{0}] is out of index range. As a result, we'll just activate the first scene instead.");
		//		return sceneAssets[0].name;
		//	}

		//	return sceneAssets[(sceneToActivateOnLoadCompletion)].name;
		//}
	}
}