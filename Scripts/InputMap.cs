﻿using UnityEngine;

using System;

namespace Vanilla.Inputs {
	[CreateAssetMenu(fileName = "New Input Map", menuName = "Vanilla/Input Map")]
	[Serializable]
	public class InputMap : ScriptableObject {
		[Tooltip("This is the name used to specify which inputMap is being requested from the InputMapDictionary. This can have a generic name like 'Player Four', something expectable in code.")]
		public string dictionaryName;

		public KeyInputConfig[] keys;
		public ButtonInputConfig[] buttons;
		public AxisInputConfig[] axes;
		public MouseAxisInputConfig[] mouseAxes;

		[Serializable]
		public class InputConfig {
			[Tooltip("A developer-friendly name for easy reading")]
			[SerializeField]
			public string name;

			public virtual bool HasAtLeastOneResponsibility() {
				return false;
			}
		}

		[Serializable]
		public class KeyInputConfig : InputConfig {
			[SerializeField]
			public KeyCode keyCode;

			public ProjectAction[] buttonPressedActions;
			public ProjectAction[] buttonHeldActions;
			public ProjectAction[] buttonReleasedActions;
			public ProjectAction[] buttonChangedActions;
			public ProjectAction[] buttonUpdateActions;

			public override bool HasAtLeastOneResponsibility() {
				return buttonPressedActions.Length > 0 || buttonReleasedActions.Length > 0 || buttonChangedActions.Length > 0 || buttonUpdateActions.Length > 0 || buttonHeldActions.Length > 0; 
			}
		}

		[Serializable]
		public class ButtonInputConfig : InputConfig {
			[Tooltip("According to Unity, which button is this?")]
			[SerializeField]
			public InputID unityButtonID;

			public ProjectAction[] buttonPressedActions;
			public ProjectAction[] buttonHeldActions;
			public ProjectAction[] buttonReleasedActions;
			public ProjectAction[] buttonChangedActions;
			public ProjectAction[] buttonUpdateActions;

			public override bool HasAtLeastOneResponsibility() {
				return buttonPressedActions.Length > 0 || buttonReleasedActions.Length > 0 || buttonChangedActions.Length > 0 || buttonUpdateActions.Length > 0 || buttonHeldActions.Length > 0;
			}
		}

		[Serializable]
		public class AxisInputConfig : InputConfig {
			[Tooltip("According to Unity, which axis is this?")]
			[SerializeField]
			public InputID unityAxisID;

			[SerializeField]
			public float changeThreshold;
			[SerializeField]
			public float polarityThreshold;

			public bool invert;
			//public bool raw;

			/// <summary>
			/// Which ProjectActions, when they become available, should subscribe to this axis via a Float event? Note that for this to work, the callback function needs to accept a float parameter.
			/// </summary>
			public ProjectAction[] axisUpdateActions;
			public ProjectAction[] axisChangedActions;
			public ProjectAction[] axisBecamePositiveActions;
			public ProjectAction[] axisBecameNeutralActions;
			public ProjectAction[] axisBecameNegativeActions;

			public override bool HasAtLeastOneResponsibility() {
				return axisUpdateActions.Length > 0 || axisChangedActions.Length > 0 || axisBecamePositiveActions.Length > 0 || axisBecameNeutralActions.Length > 0 || axisBecameNegativeActions.Length > 0;
			}
		}

		[Serializable]
		public class MouseAxisInputConfig : InputConfig {
			public MouseAxisType mouseInputType;

			[Tooltip("For the axisChanged event, how much difference does there need to be between frames before the event fires?")]
			[SerializeField]
			public float changeThreshold;

			public ProjectAction[] axisUpdateActions;
			public ProjectAction[] axisChangedActions;

			public override bool HasAtLeastOneResponsibility() {
				return axisUpdateActions.Length > 0 || axisChangedActions.Length > 0;
			}
		}
	}
}