﻿using UnityEngine;

namespace Vanilla.EventSystems {
	public class PointerEventCanvas : VanillaBehaviour {

		[HideInInspector]
		public Canvas canvas;

		void Awake() {
			canvas = GetComponent<Canvas>();
		}

		public void OnEnable() {
			CanvasPointerModule.canvas = canvas;
		}

		public void OnDisable() {
			if (CanvasPointerModule.canvas == this) {
				CanvasPointerModule.canvas = null;
			}
		}
	}
}
