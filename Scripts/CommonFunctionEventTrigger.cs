﻿using UnityEngine;
using UnityEngine.Events;

namespace Vanilla {
	public class CommonFunctionEventTrigger : VanillaBehaviour {

		///------------------------------------------------------------------------------------
		/// This base class is for invoking functions based on common Unity callbacks.
		/// It's easily inherited from and should theoretically prevent a lot of basic
		/// class re-writing. You can simply plug public events into the onConditionsMet
		/// event or override it via inheritance.
		///------------------------------------------------------------------------------------

		[Header("Conditions")]
		public CommonUnityFunction whenToExecute;

		public bool canRunMoreThanOnce;

		public bool hasFired;

		[Header("Events")]
		public UnityEvent onConditionsMet;

		void Awake() {
			Execute(CommonUnityFunction.Awake);
		}

		void Start() {
			Execute(CommonUnityFunction.Start);
		}

		void OnEnable() {
			Execute(CommonUnityFunction.OnEnable);
		}

		void OnDisable() {
			Execute(CommonUnityFunction.OnDisable);
		}

		void OnBecameVisible() {
			Execute(CommonUnityFunction.OnBecameVisible);
		}

		void OnBecameInvisible() {
			Execute(CommonUnityFunction.OnBecameInvisible);
		}

		void Execute(CommonUnityFunction currentFunction) {
			// Back out early if the loadMode doesn't match this instances, or if we've already loaded before and repeat loading isn't allowed.
			if (currentFunction != whenToExecute || (!canRunMoreThanOnce && hasFired)) {
				Warning("This EventTriggers conditions [{0}] failed because it doesn't match the desired conditions [{1}]; no Load will happen this time as a result.", currentFunction, whenToExecute);
				return;
			}

			hasFired = true;

			LoadConditionsMet();
		}

		public virtual void LoadConditionsMet() {
			Log("Load conditions met for the CommonFunctionEventTrigger attached to [{0}]", gameObject.name);

			hasFired = true;

			onConditionsMet.Invoke();
		}
	}
}