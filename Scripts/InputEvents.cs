﻿//using UnityEngine;
//using UnityEngine.Events;

//using Vanilla;
//using Vanilla.CustomEvents;

//namespace Input {

//	public class InputEventProfileOld : ScriptableObject {

//		public InputEvent[] events;

//		public UnityEvent AButtonPressed = new UnityEvent();
//		public UnityEvent AButtonDown = new UnityEvent();
//		public UnityEvent AButtonReleased = new UnityEvent();

//		public UnityEvent BButtonPressed = new UnityEvent();
//		public UnityEvent BButtonDown = new UnityEvent();
//		public UnityEvent BButtonReleased = new UnityEvent();

//		public UnityEvent XButtonPressed = new UnityEvent();
//		public UnityEvent XButtonDown = new UnityEvent();
//		public UnityEvent XButtonReleased = new UnityEvent();

//		public UnityEvent YButtonPressed = new UnityEvent();
//		public UnityEvent YButtonDown = new UnityEvent();
//		public UnityEvent YButtonReleased = new UnityEvent();

//		public UnityEvent L1ButtonPressed = new UnityEvent();
//		public UnityEvent L1ButtonDown = new UnityEvent();
//		public UnityEvent L1ButtonReleased = new UnityEvent();

//		public UnityEvent R1ButtonPressed = new UnityEvent();
//		public UnityEvent R1ButtonDown = new UnityEvent();
//		public UnityEvent R1ButtonReleased = new UnityEvent();

//		public UnityEvent L2ButtonPressed = new UnityEvent();
//		public UnityEvent L2ButtonDown = new UnityEvent();
//		public UnityEvent L2ButtonReleased = new UnityEvent();

//		public UnityEvent R2ButtonPressed = new UnityEvent();
//		public UnityEvent R2ButtonDown = new UnityEvent();
//		public UnityEvent R2ButtonReleased = new UnityEvent();

//		public UnityEvent StartButtonPressed = new UnityEvent();
//		public UnityEvent StartButtonReleased = new UnityEvent();

//		public UnityEvent SelectButtonPressed = new UnityEvent();
//		public UnityEvent SelectButtonReleased = new UnityEvent();

//		public UnityEvent FixedUpdateEvent = new UnityEvent();
//		public UnityEvent UpdateEvent = new UnityEvent();

//		public float h;
//		public float v;

//		public bool ADown;
//		public bool BDown;
//		public bool XDown;
//		public bool YDown;
//		public bool L1Down;
//		public bool R1Down;
//		public bool L2Down;
//		public bool R2Down;

//		static float deltaH;

//		void Awake() {
//			if (Input.inputProfiles[playerID]) {
//				Error("This input profile [{0}] is already registered, please try a different playerID or correct my existence.", playerID);
//				Destroy(this);
//			}

//			Input.inputProfiles[playerID] = this;
//		}

//		void FixedUpdate() {
//			if (Vanilla.i.paused) {
//				return;
//			}

//			FixedUpdateEvent.Invoke();
//		}

//		public void Update() {
//			HandleInput();

//			if (paused) {
//				return;
//			}

//			UpdateEvent.Invoke();
//		}

//		public void HandleInput() {
//			#region Start & Select Buttons
//			if (UnityEngine.Input.GetButtonDown("Start")) {
//				StartButtonPressed.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("Start")) {
//				StartButtonReleased.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonDown("Select")) {
//				SelectButtonPressed.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("Select")) {
//				SelectButtonReleased.Invoke();
//			}
//			#endregion

//			if (paused) {
//				return;
//			}

//			#region Joystick Axes
//			h = UnityEngine.Input.GetAxisRaw("Horizontal");
//			v = UnityEngine.Input.GetAxisRaw("Vertical");

//			if (v != 0) {
//				Log("Vertical Input Axis [" + v + "]");
//			}
//			#endregion

//			#region A Button
//			if (UnityEngine.Input.GetButtonDown("A")) {
//				AButtonPressed.Invoke();

//				ADown = true;

//				Log("A Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("A")) {
//				AButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("A")) {
//				AButtonReleased.Invoke();

//				ADown = false;

//				Log("A Button Released");
//			}
//			#endregion

//			#region B Button
//			if (UnityEngine.Input.GetButtonDown("B")) {
//				BButtonPressed.Invoke();

//				BDown = true;

//				Log("B Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("B")) {
//				BButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("B")) {
//				BButtonReleased.Invoke();

//				BDown = false;

//				Log("B Button Released");
//			}
//			#endregion

//			#region X Button
//			if (UnityEngine.Input.GetButtonDown("X")) {
//				XButtonPressed.Invoke();

//				XDown = true;

//				Log("X Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("X")) {
//				XButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("X")) {
//				XButtonReleased.Invoke();

//				XDown = false;

//				Log("X Button Released");
//			}
//			#endregion

//			#region Y Button
//			if (UnityEngine.Input.GetButtonDown("Y")) {
//				YButtonPressed.Invoke();

//				YDown = true;

//				Log("Y Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("Y")) {
//				YButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("Y")) {
//				YButtonReleased.Invoke();

//				YDown = false;

//				Log("Y Button Released");
//			}
//			#endregion

//			#region L1 Button
//			if (UnityEngine.Input.GetButtonDown("L1")) {
//				L1ButtonPressed.Invoke();

//				L1Down = true;

//				Log("L1 Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("L1")) {
//				L1ButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("L1")) {
//				L1ButtonReleased.Invoke();

//				L1Down = false;

//				Log("L1 Button Released");
//			}
//			#endregion

//			#region R1 Button
//			if (UnityEngine.Input.GetButtonDown("R1")) {
//				R1ButtonPressed.Invoke();

//				R1Down = true;

//				Log("R1 Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("R1")) {
//				R1ButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("R1")) {
//				R1ButtonReleased.Invoke();

//				R1Down = false;

//				Log("R1 Button Released");
//			}
//			#endregion

//			#region L2 Button
//			if (UnityEngine.Input.GetButtonDown("L2")) {
//				L2ButtonPressed.Invoke();

//				L2Down = true;

//				Log("L2 Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("L2")) {
//				L2ButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("L2")) {
//				L2ButtonReleased.Invoke();

//				L2Down = false;

//				Log("L2 Button Released");
//			}
//			#endregion

//			#region R2 Button
//			if (UnityEngine.Input.GetButtonDown("R2")) {
//				R2ButtonPressed.Invoke();

//				R2Down = true;

//				Log("R2 Button Pushed");
//			}

//			if (UnityEngine.Input.GetButton("R2")) {
//				R2ButtonDown.Invoke();
//			}

//			if (UnityEngine.Input.GetButtonUp("R2")) {
//				R2ButtonReleased.Invoke();

//				R2Down = false;

//				Log("R2 Button Released");
//			}
//			#endregion
//		}

//		public static bool CheckForDirectionChange() {
//			if (h > 0) {
//				if (deltaH >= h) {
//					deltaH = h;
//					return false;
//				} else {
//					deltaH = h;
//					return true;
//				}
//			} else {
//				if (deltaH <= h) {
//					deltaH = h;
//					return false;
//				} else {
//					deltaH = h;
//					return true;
//				}
//			}
//		}
//	}
//}