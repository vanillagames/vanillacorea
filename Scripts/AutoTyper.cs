﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla.Inputs;

namespace Vanilla {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(AutoTyper))]
	public class AutoTyperEditor : Editor {

		public AutoTyper autoTyper;

		void OnEnable() {
			autoTyper = (AutoTyper)target;
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();

			if (GUILayout.Button("Begin Typing")) {
				autoTyper.BeginTyping();
			}
		}
	}
#endif


	public class AutoTyper : VanillaBehaviour {

		[Header("Function characters")]
		[Tooltip("When this character is presented, a custom interval of time can be ran instead.")]
		public char pauseChar = '~';

		[Tooltip("When this character is presented, a new line in the text will begin.")]
		public char returnChar = '|';

		[Tooltip("When this character is presented, the characterEvent event will be invoked.")]
		public char eventChar = '#';

		[Header("Options")]
		[Tooltip("If true, a small custom interval will replace the standard wait time as outlined below.")]
		public bool doPauses;

		[Tooltip("If true, events can be triggered when a specific character is encountered. The character and the event must match up for optimization purposes.")]
		public bool doEvents;

		[Header("Sound")]
		[Tooltip("The sound that will accompany each typed letter.")]
		public AudioClip typeSound;

		[Tooltip("How much random pitch deviation will be used from the base line of 0 with each typed letter?")]
		[Range(0.0f, 1.0f)]
		public float soundPitchVarianceRange;

		[Header("Events")]
		public UnityEvent onEventCharacterTyped = new UnityEvent();

		[Tooltip("Of the pre-built wait instructions found in VanillaTime, which should be used between characters and when a pause is encountered?")]
		public int perCharacterWaitID;
		public int perPauseWaitID;

		[Tooltip("If true, once its done typing a message, the AutoTyper will wait until the corresponding ProjectAction has occurred before typing the next message.")]
		public bool useActionInputToProceed;

		public ProjectAction proceedAction;

		[ReadOnly]
		public bool waitingForInput;

		Text text;

		List<string> messageBank = new List<string>();

		IEnumerator typingProcess;

		void Awake() {
			if (!text) {
				text = GetComponentInChildren<Text>();
			}
		}

		void OnEnable() {
			if (useActionInputToProceed) {
				if (VanillaInputs.i) {
					VanillaInputs.i.Subscribe(proceedAction, ProceedWithNextMessage);
				}
			}
		}

		void OnDisable() {
			if (useActionInputToProceed) {
				if (VanillaInputs.i) {
					VanillaInputs.i.Unsubscribe(proceedAction, ProceedWithNextMessage);
				}
			}
		}



		public void AddMessage(string newMessage) {
			messageBank.Add(newMessage);
		}

		public void BeginTyping() {
			if (typingProcess != null) {
				StopCoroutine(typingProcess);
			}

			typingProcess = TypeText();

			StartCoroutine(typingProcess);
		}

		//public void NewMessage(string newMessage) {
		//	AddMessage(newMessage);

		//	BeginTyping();
		//}

		IEnumerator TypeText() {
			text.text = "";

			char c;

			while (messageBank.Count > 0) {
				char[] characters = messageBank[0].ToCharArray();

				for (int i = 0; i < characters.Length; i++) {
					c = characters[i];

					if (c == pauseChar) {
						yield return VanillaTime.i.GetUnscaledWait(perPauseWaitID);
					} else if (c == returnChar) {
						text.text += "\n";
						yield return VanillaTime.i.GetUnscaledWait(perCharacterWaitID);
					} else if (c == eventChar) {

					} else {
						text.text += c;
						VanillaAudio.i.PlayClipWithRandomPitchModulation(typeSound, soundPitchVarianceRange);
						yield return VanillaTime.i.GetUnscaledWait(perCharacterWaitID);
					}
				}

				messageBank.RemoveAt(0);

				if (useActionInputToProceed) {
					waitingForInput = true;

					// Activate the 'press-a-button-to-continue' sprite in the dialogue box or similar stuff here

					while (waitingForInput) {
						yield return null;
					}
				}
			}
		}

		public void ProceedWithNextMessage() {
			waitingForInput = false;
		}

		//IEnumerator TypeTextOld() {
		//	text.text = "";
		//	char[] characters = message.ToCharArray();
		//	WaitForSeconds charWait = new WaitForSeconds(secondsBetweenCharacters);

		//	if (doPauses && doEvents) {
		//		WaitForSeconds pauseWait = new WaitForSeconds(secondsWhenPausing);
		//		char c;

		//		for (int i = 0; i < characters.Length; i++) {
		//			c = characters[i];

		//			if (pauseCharacterCheck(c)) {
		//				yield return pauseWait;
		//			} else if (returnCharacterCheck(c)) {
		//				yield return charWait;
		//			} else if (eventCharacterCheck(c)) {
		//				yield return charWait;
		//			} else {
		//				appendText(c);
		//				yield return charWait;
		//			}
		//		}
		//	} else if (doPauses) {
		//		WaitForSeconds pauseTime = new WaitForSeconds(secondsWhenPausing);
		//		char c;

		//		for (int i = 0; i < characters.Length; i++) {
		//			c = characters[i];

		//			if (pauseCharacterCheck(c)) {
		//				yield return pauseTime;
		//			} else if (returnCharacterCheck(c)) {
		//				yield return charWait;
		//			} else {
		//				appendText(c);
		//				yield return charWait;
		//			}
		//		}
		//	} else if (doEvents) {
		//		char c;

		//		for (int i = 0; i < characters.Length; i++) {
		//			c = characters[i];

		//			if (returnCharacterCheck(c)) {
		//				yield return charWait;
		//			} else if (eventCharacterCheck(c)) {
		//				yield return charWait;
		//			} else {
		//				appendText(c);
		//				yield return charWait;
		//			}
		//		}
		//	} else {
		//		for (int i = 0; i < characters.Length; i++) {
		//			appendText(characters[i]);
		//			yield return charWait;
		//		}
		//	}

		//	yield return new WaitForSeconds(secondsToRead);

		//	onTypingCompleted.Invoke();

		//	//if (fadeWhenFinished) {
		//	//	StartCoroutine("FadeElements");
		//	//}
		//}

		//public void BeginFadeOut() {
		//	StartCoroutine("FadeElements");
		//}

		//public IEnumerator FadeElements() {
		//	float i = 1.0f;

		//	Color backingColor = backing.color;
		//	Color textColor = text.color;

		//	while (i > 0.0f) {
		//		i -= Time.fixedDeltaTime;
		//		//i -= Time.fixedDeltaTime * 0.5f;

		//		backingColor.a = i;
		//		textColor.a = i;

		//		backing.color = backingColor;
		//		text.color = textColor;

		//		yield return null;
		//	}

		//	//if (resetColorOnFinish) {
		//		backingColor.a = 1.0f;
		//		textColor.a = 1.0f;

		//		backing.color = backingColor;
		//		text.color = textColor;
		//	//}

		//	onFadeCompleted.Invoke();
		//}

		//bool pauseCharacterCheck(char c) {
		//	return c == pauseChar;
		//}

		bool eventCharacterCheck(char c) {
			if (c == eventChar) {
				onEventCharacterTyped.Invoke();
				return true;
			} else {
				return false;
			}
		}

		bool returnCharacterCheck(char c) {
			if (c == returnChar) {
				text.text += "\n";
				return true;
			} else {
				return false;
			}
		}

		void appendText(char c) {
			text.text += c;
		}
	}
}