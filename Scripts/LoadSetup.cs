﻿using UnityEngine;
using System.Collections;

using Vanilla;

public class LoadSetup : MonoBehaviour {

	public SceneSetup setup;

	public void Load() {
		if (VanillaSceneLoader.i) {
			VanillaSceneLoader.i.LoadSetup(setup);
		}
	}
}
