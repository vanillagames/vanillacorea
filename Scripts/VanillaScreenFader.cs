﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using System.Collections;

namespace Vanilla {
#if UNITY_EDITOR
	using UnityEditor;

[CustomEditor(typeof(VanillaScreenFader))]
public class ScreenFaderEditor : VanillaEditor {
	
	VanillaScreenFader fader;
	
	public void OnEnable() {
		fader = (VanillaScreenFader)target;
	}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (Application.isPlaying) {
				if (GUILayout.Button("Update fader property")) {
					fader.fadeNormal = fader.fadeNormal;
				}
			}else {
				if (GUILayout.Button("Run Initialize")) {
					if (!fader.initialized) {
						fader.StartCoroutine("Initialize");
						fader.initialized = true;
					}
				}

				//if (GUILayout.Button("Use Default Fader Settings")) {
				//	fader.meshLayer = LayerMask.NameToLayer(fader.nickname);
				//}

//				if (GUILayout.Button("Use Default Camera Settings")) {
//<<<<<<< HEAD
//					fader.faderCam.cam.cullingMask = LayerMask.GetMask(fader.nickname);
//=======
//					fader.faderCam.cam.cullingMask = LayerMask.GetMask(fader.faderCam.cameraNickname);
//>>>>>>> 128aa6b7f5cae7e93cf08b87d58f0be35ea211a1

//					fader.faderCam.cam.orthographic = true;

//					fader.faderCam.cam.orthographicSize = 0.1f;

//					// The reasoning for this is that the position passed on update can be simply transform.position, allowing the camera to move anywhere and never affect the draw.
//					fader.faderCam.cam.nearClipPlane = -0.1f;
//					fader.faderCam.cam.farClipPlane = 0.1f;
//				}
			}

			 
		}
	}
#endif

	public class VanillaScreenFader : VanillaSystemModule<VanillaScreenFader> {

		[Header("Screen Fader")]
		//[Header("Camera Material")]
		//[Tooltip("Populate this with a material that uses the FaderShader shader.")]
		//public Material faderMaterial;

		//[ReadOnly]
		//public VanillaCamera faderCam;

		//[Tooltip("Which transform, if any, should the fade mesh be drawn in front of? Passing a Camera in with DrawMesh wasn't working, so we allow ourselves to nominate a camera to sit in front of instead.")]
		//public Transform drawTransform;

		//[Tooltip("This should be Unitys default quad mesh. Unfortunately there is no way to set this automatically.")]
		//public Mesh faderQuad;

		//[Tooltip("Which term should we use to find references for the fader? This is used in the CameraDictionary and Unitys layer list.")]
		//public string nickname = "ScreenFader";

		//[HideInInspector]
		//public int meshLayer = -1;

		[Header("Fade Image")]
		public Image _fadeImage;
		public Image fadeImage {
			get {
				if (!_fadeImage) {
					_fadeImage = GetComponent<Image>();

					if (!_fadeImage) {
						Error("I have no assigned image to fade and no image component is attached. This is a disaster!");
					}
				}

				return _fadeImage;
			}
			set {
				_fadeImage = value;
			}
		}

		[Header("Fade Colour")]
		public Color fadeColor;

		//[Header("Debugging")]
		//public float posOffset;
		//public Vector3 eulerOffset;

		[Header("Timing")]
		[Range(0, 1)]
		public float _fadeNormal;
		public float fadeNormal {
			get {
				return _fadeNormal;
			}
			set {
				// We don't need to worry about values being out of range this way.
				_fadeNormal = Mathf.Clamp01(value);

				// Set the color alpha.
				fadeColor.a = value;

				fadeImage.color = fadeColor;

				fadeImage.enabled = enabled = _fadeNormal > 0;

				// Set the whole material color.
				//faderMaterial.color = fadeColor;


				// Enable both the camera and fader components if the new value is greater than 0.
				//faderCam.enabled = faderCam.cam.enabled = enabled = _fadeNormal > 0;

			}
		}

		public override IEnumerator Initialize() {
			i = this;

			//yield return base.Initialize();

			// This creates an instance of the faderMaterial which we'll use instead.
			// Previously, it would edit the material asset itself and changes would remain after exiting PlayMode.
			//faderMaterial = new Material(faderMaterial);

			// We need to elect a layer to draw the fader quad on; there's no option not to. However, having it on a layer lets us have more control over draw depth, so lets use it.
			// We need a layer called ScreenFader and thats all!
			//if (meshLayer == -1) {
				//meshLayer = LayerMask.NameToLayer(nickname);

			//	if (meshLayer == -1) {
			//		Error("The screen fader requires a layer to itself simply called 'ScreenFader', but none was found in the project. ScreenFader functionality may not work as expected this time around.");
			//	}
			//}

			// Preset the camera reference to skip this GetComponent()
			//if (!faderCam) {
				//faderCam = GetComponent<VanillaCamera>();
				//faderCam = VanillaCameras.i.GetVanillaCamera(nickname);
				//faderCam = VanillaCameras.i.GetVanillaCamera("ScreenFader");

				//if (faderCam == null) {
				//	Error("No camera by the name [{0}] exists in the dictionary.");
				//	yield break;
				//}
			//}

			// This runs the setter code, enabling or disabling the component as required.
			fadeNormal = fadeNormal;

			gameObject.SetActive(true);

			yield break;
		}

		//public void OnEnable() {
		//	if (!initialized) {
		//		enabled = false;
		//	}
		//}

		//public override void Update() {
		//	fadeImage.color = 
		//}

		//public override void Update() {
		//	if (drawTransform) {
		//		Graphics.DrawMesh(faderQuad, drawTransform.position + (drawTransform.forward * posOffset), Quaternion.Euler(drawTransform.eulerAngles + eulerOffset), faderMaterial, meshLayer);
		//	} else {
		//		Graphics.DrawMesh(faderQuad, new Vector3(0,0,posOffset), Quaternion.Euler(transform.eulerAngles + eulerOffset), faderMaterial, meshLayer);
		//	}
		//}
	}
}