﻿using UnityEngine;

using System;
using System.Collections;

using Vanilla.Transitions;

namespace Vanilla {
	public class LeadScene : VanillaScene {

		// Lead scenes are made to be activated and have transition asset fields ready to go

		[Header("[ Lead Scene ]")]

		[SerializeField]
		public TransitionConfiguration inTransition;

		[SerializeField]
		public TransitionConfiguration outTransition;

		public override IEnumerator Activate() {
			if (inTransition != null && inTransition.asset != null) {
				yield return VanillaTransitions.i.Animate(inTransition);
			}

			yield break;
		}

		public override IEnumerator Deactivate() {
			if (outTransition != null && outTransition.asset != null) {
				yield return VanillaTransitions.i.Animate(outTransition);
			}

			yield break;
		}
	}
}