﻿using UnityEngine;

using System.Collections;

namespace Vanilla {
	public class VanillaSystemCanvas : VanillaSystemModule<VanillaSystemCanvas> {

		[HideInInspector]
		public Canvas canvas;

		public override IEnumerator Initialize() {
			canvas = GetComponent<Canvas>();

			return base.Initialize();
		}

		public override void SystemInitializationComplete() {

		}
	}
}