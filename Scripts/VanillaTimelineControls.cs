﻿using UnityEngine;

using System;

using Vanilla;

public class VanillaTimelineControls : VanillaBehaviour {

	public float timeMultiplier = 1.0f;

	public VanillaTimeline timeline;

	void Update() {
		if (timeline) {
			timeline.AddTime(Time.deltaTime * timeMultiplier);
		}
	}
}