﻿//using UnityEngine.Events;
//using Vanilla.VanillaInput;

//namespace Vanilla.VanillaVR {
//	public class ButtonEventSet : InputEventSet {

//		public UnityEvent onButtonPressed;
//		public UnityEvent onButtonHeld;
//		public UnityEvent onButtonReleased;

//		public void Awake() {
//			if (onButtonPressed == null) {
//				onButtonPressed = new UnityEvent();
//			}

//			if (onButtonHeld == null) {
//				onButtonHeld = new UnityEvent();
//			}

//			if (onButtonReleased == null) {
//				onButtonReleased = new UnityEvent();
//			}
//		}

//		public override void OnEnable() {
//			base.OnEnable();

//			VanillaController.GetController(inputModuleSubscription.controllerName).buttonDictionary[inputModuleSubscription.inputFieldName].onButtonEvent.AddListener(HandleButtonEvents);
//		}

//		public override void OnDisable() {
//			VanillaController.GetController(inputModuleSubscription.controllerName).buttonDictionary[inputModuleSubscription.inputFieldName].onButtonEvent.RemoveListener(HandleButtonEvents);
//		}

//		public void HandleButtonEvents(ButtonState buttonState) {
//			switch (buttonState) {
//				case ButtonState.Pressed:
//					onButtonPressed.Invoke();
//					break;
//				case ButtonState.Held:
//					onButtonHeld.Invoke();
//					break;
//				case ButtonState.Released:
//					onButtonReleased.Invoke();
//					break;
//			}
//		}
//	}
//}