﻿//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.EventSystems;

//using System.Collections.Generic;

//namespace Vanilla.EventSystems {
//	public class CanvasPointer : BasePointerOLD {

//		// References
//		public static Canvas canvas;

//		Camera _camera;
//		new Camera camera {
//			get {
//				if (!_camera) {
//					_camera = GetComponent<Camera>();

//					if (!_camera) {
//						Error("Something (probably this script) attempted to access the camera but it is unassigned and the assignment attempt failed.");
//					}
//				}

//				return _camera;
//			}
//		}

//		List<RaycastResult> results = new List<RaycastResult>();

//		RectTransform hitRect;

//		[HideInInspector]
//		Vector3 reticleOriginalScale;

//		public override void Awake() {
//			base.Awake();

//			if (reticle) {
//				reticleOriginalScale = reticle.transform.localScale;
//			}
//		}

//		public override void OnEnable() {
//			base.OnEnable();
//		}

//		public override void OnDisable() {
//			base.OnDisable();
//		}

//		public override void FireRay() {
//			// Just in case you lose track of the idea!
//			// The idea is that each Pointer keeps track of its 'current' hit and compares it with the currently selected object but only on 'enter'.
//			// i.e. only when the ray has encountered that object for the 'first' time, or has come from nothing.
//			// So if a ray hits nothing, and then an object, it would compare it with the currently selected one. If its the same, ignore. If its not, send it to EventSystems.SelectNewObject
//			// If a ray hits the same object, ignore it.
//			// If a ray hits nothing, but has a local hit cached, compare it with whats currently selected. If its a match, tell EventSystems that its been unselected. I think you do this by saying EventSystems.SelectNewObject(null)
//			// I advise still sending OnPointerEnter and OnPointerExit (only if its -not- a match?) since OnSelect was weird for reasons I can't remember.

//			base.FireRay();

//			if (!canvas) {
//				return;
//			}

//			canvas.worldCamera = camera;

//			// This works in an interesting way. As far as I can tell, it iterates through each raycast-target UI element in order of the hierarchy (!) which might mean we need to do our own proximity/depth checking.
//			EventSystem.current.RaycastAll(pointerEventData, results);

//			// If the results returned anything
//			if (results.Count > 0) {
//				// See what we should do with it.
//				AssessRayResult(results[0].gameObject);
//			} else {
//				// We use the same function to handle 'nothing' and empty our cached hit results.
//				AssessRayResult(null);
//			}

//			// The way UI element results work are a bit different to normal raycast results, so we have to handle them seperately.
//			switch (rayResult) {
//				case PointerResult.NoHit:

//					break;
//				case PointerResult.HitEnter:
//					hitRect = currentHit.transform as RectTransform;
//					break;
//				case PointerResult.HitExit:
//					hitRect = null;
//					break;
//				case PointerResult.HitSwap:
//					hitRect = currentHit.transform as RectTransform;
//					break;
//				case PointerResult.HitSame:

//					break;
//			}

//			// With our hit references cached or empty, we can finally instruct the line renderer and reticle (if either) on what to do.
//			if (hitRect) {
//				// We use a special function to find the worldspace hit position on a world-space UI element (they don't have colliders!)
//				GetRectHitPosition();
//			} else {
//				// If we didn't hit anything, use the default 
//				UseDefaultHitDistance();
//			}

//			if (line) {
//				// Move the line renderer positions to match the pointer and the hit position.
//				UpdateLinePositions();
//			}

//			if (reticle) {
//				// Move the reticle object to the hit position.
//				UpdateReticlePosition();

//				// Do different things depending on our reticleRotationMode setting.
//				switch (reticleRotationMode) {
//					case ReticleRotationMode.AlignToHit:
//						// If we have a hit, use the rotation of that UI element.
//						if (hitRect) {
//							GetRectHitRotation();

//							reticle.rotation = hitRotation;
//						} else {
//							// Otherwise, fallback to just looking at the camera.
//							MakeReticleLookAtCamera();
//						}
//						break;
//					case ReticleRotationMode.LookAtMainCamera:
//						MakeReticleLookAtCamera();
//						break;
//				}

//				ScaleReticle();
//			}
//		}

//		public override void BuildPointerEventData() {
//			base.BuildPointerEventData();

//			pointerEventData.scrollDelta = Vector2.zero; // How is this meant to be implemented? Dragging elements could be fun.

//			pointerEventData.position = new Vector2(camera.pixelWidth * 0.5f, camera.pixelHeight * 0.5f);
//		}

//		void UseDefaultHitDistance() {
//			hitPosition = transform.position + (transform.forward * defaultRayLength);
//		}

//		void GetRectHitPosition() {
//			RectTransformUtility.ScreenPointToWorldPointInRectangle(hitRect, pointerEventData.position, camera, out hitPosition);
//		}

//		void GetRectHitRotation() {
//			hitRotation = hitRect.rotation;
//		}

//		void ScaleReticle() {
//			if (cameraTransform != null) {
//				reticle.localScale = reticleOriginalScale * Vector3.Distance(hitPosition, cameraTransform.position);
//			}
//		}

//		void MakeReticleLookAtCamera() {
//			if (cameraTransform != null) {
//				reticle.transform.LookAt(cameraTransform);
//			}
//		}

//		//public void OnDrawGizmos() {
//		//	if (Application.isPlaying) {
//		//		Gizmos.DrawSphere(reticlePosition, 0.25f);
//		//	}
//		//}
//	}
//}