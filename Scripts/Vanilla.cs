﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla.EventSystems;
using Vanilla.CustomEvents;
using Vanilla.Inputs;

namespace Vanilla {

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// VanillaCore Editor
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	#region VanillaCore Editor
	#if UNITY_EDITOR
	using UnityEditor;
	using UnityEditor.SceneManagement;

	[CustomEditor(typeof(Vanilla))]
	public class VanillaCoreEditor : VanillaEditor {

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();
			
			if (GUILayout.Button("Turn off all [Log] DebugFlags")) {
				VanillaBehaviour[] vanillaBehaviours = FindObjectsOfType<VanillaBehaviour>();

				for (int i = 0; i < vanillaBehaviours.Length; i++) {
					//vanillaBehaviours[i].debugFlags = vanillaBehaviours[i].debugFlags & (~DebugFlags.Logs);

					// This unsets the log flag?
					VanillaDebug.UnsetFlag(vanillaBehaviours[i].debugFlags, DebugFlags.Logs);

					//EditorUtility.SetDirty(vanillaBehaviours[i]);

					//if (!Application.isPlaying) {
					//	EditorSceneManager.MarkSceneDirty(vanillaBehaviours[i].gameObject.scene);
					//}
				}
			}

			if (GUILayout.Button("Turn on all [Log] DebugFlags")) {
				VanillaBehaviour[] vanillaBehaviours = FindObjectsOfType<VanillaBehaviour>();

				for (int i = 0; i < vanillaBehaviours.Length; i++) {
					//vanillaBehaviours[i].localDebugMode = DebugMode.All;

					VanillaDebug.SetFlag(vanillaBehaviours[i].debugFlags, DebugFlags.Logs);
				}
			}

			if (GUILayout.Button("Turn off all DebugFlags except [Errors]")) {
				VanillaBehaviour[] vanillaBehaviours = FindObjectsOfType<VanillaBehaviour>();

				for (int i = 0; i < vanillaBehaviours.Length; i++) {
					//vanillaBehaviours[i].localDebugMode = DebugMode.ErrorsOnly;

					vanillaBehaviours[i].debugFlags = 0;
					VanillaDebug.SetFlag(vanillaBehaviours[i].debugFlags, DebugFlags.Errors);
				}
			}
		}
	}
#endif
	#endregion

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Force the 'System' scene to be active on start (to make it run first!)
	///--------------------------------------------------------------------------------------------------------------------------------------------------

#if UNITY_EDITOR
	[InitializeOnLoad]
	public static class ForceSystemSceneActive {

		public static bool forceSystemSceneActiveOnStart = true;

		static ForceSystemSceneActive() {
			EditorApplication.playModeStateChanged += HandlePlayModeChange;
		}

		private static void HandlePlayModeChange(PlayModeStateChange state) {
			if (!forceSystemSceneActiveOnStart) {
				return;
			}

			switch (state) {
				case PlayModeStateChange.ExitingEditMode:
					Vanilla vanillaInstance = GameObject.FindObjectOfType<Vanilla>();

					if (vanillaInstance == null) {
						// If there's no instance of Vanilla loaded, that means System isn't loaded and can't be activated.
						return;
					} else {
						SceneManager.SetActiveScene(vanillaInstance.gameObject.scene);
					}
					break;
			}
		}
	}
#endif

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// VanillaCore
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	public class Vanilla : VanillaSystemModule<Vanilla> {

		[Header("[ Vanilla ]")]
		[Tooltip("If true, Vanilla will load the System by itself at the earliest possible moment.")]
		public bool closeAllOpenScenesWhenEnteringPlayMode;

		[Tooltip("If this is populated with a Text object, all debug logs will be printed there as well.")]
		public Text worldSpaceConsole;

		[Tooltip("If true, timeScale will be set to 0 until all system modules have been initialized. This allows us to assume the positions of things during setup, if need be. However, it is possible that time may be required for certain parts of setup in the future, so tread carefully.")]
		public bool freezeTimeUntilSystemSyncComplete;

		[Tooltip("This is the desired frame rate for the program. -1 means use the platforms preferred frame-rate, but a custom rate can be supplied instead.")]
		public int initialTargetFrameRate = -1;

		public int worldCanvasDebugLogMax;

		public VanillaSystemModuleBase[] systemModules;

		[Tooltip("This is set to true automatically when Restart or Quit is called and prevents all subsequent logging.")]
		public static bool applicationQuitBegun = false;

		void OnEnable() {
			if (inUse) {
				if (!initialized) {
					if (closeAllOpenScenesWhenEnteringPlayMode && SceneManager.sceneCount > 1) {
						Restart();
						return;
					}

					StartCoroutine("InitializeSystem");
				}
			} else {
				Warning("Vanilla is currently set to not be in use, so no scene or system management will occur.");
			}
		}

		/// <summary>
		/// Vanilla kicks off here. All system modules are booted in order, starting with this one!
		/// </summary>
		public IEnumerator InitializeSystem() {
			applicationQuitBegun = false;

			systemModules = GetComponentsInChildren<VanillaSystemModuleBase>(true);

			Log("Beginning Vanilla initialization.");

			for (int i = 0; i < systemModules.Length; i++) {
				if (systemModules[i].inUse) {
					systemModules[i].gameObject.SetActive(true);

					Log("Initializing system module [{0}].", systemModules[i].name);

					systemModules[i].AssignSingleton();

					yield return systemModules[i].Initialize();

					systemModules[i].initialized = true;

					Log("Initialization complete for the [{0}] system module.", systemModules[i].name);
				} else {
					systemModules[i].gameObject.SetActive(false);
				}
			}

			Log("All modules initialized! Firing system-wide synchronization to kick things off.");

			if (freezeTimeUntilSystemSyncComplete) {
				Time.timeScale = 1.0f;
			}

			for (int i = 0; i < systemModules.Length; i++) {
				if (systemModules[i].inUse) {
					systemModules[i].SystemInitializationComplete();
				}
			}
		}

		/// <summary>
		/// Vanilla kicks off here.
		/// </summary>
		public override IEnumerator Initialize() {
			if (freezeTimeUntilSystemSyncComplete) {
				Time.timeScale = 0;
			}

			if (initialTargetFrameRate != -1) {
				UpdateTargetFrameRate(initialTargetFrameRate);
			}

			// This should be in its own module
			//if (worldSpaceConsole) {
			//	VanillaLog.worldConsole = worldSpaceConsole;
			//	VanillaLog.logMax = worldCanvasDebugLogMax;
			//}

			yield break;
		}

		/// <summary>
		/// Closes all open scenes by loading only the System scene. This effectively restarts the engine and thus the project, but it shouldn't be used
		/// to restart the project. It's primary function is to close all open scenes in the editor when starting play mode.
		/// </summary>
		public static void Restart() {
			VanillaLog.Log("Rebooting Vanilla...");

			applicationQuitBegun = true;

			SceneManager.LoadScene(0);
		}

		public void OnApplicationQuit() {
			if (!applicationQuitBegun) {
				applicationQuitBegun = true;
			} else {
				Error("Multiple quit requests made. People, please, calm down!");
			}
		}

		public void UpdateTargetFrameRate(int newFrameRate) {
			Application.targetFrameRate = newFrameRate;
		}
	}

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Custom Events
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	namespace CustomEvents {
		public class StringEvent : UnityEvent<string> {}
		public class IntEvent : UnityEvent<int> {}
		public class BoolEvent : UnityEvent<bool> {}
		public class FloatEvent : UnityEvent<float> {}
		public class Vector3Event : UnityEvent<Vector3> {}

		//public class MultipleParameterExampleEvent : UnityEvent<float, Vector3, string> {}

		//public class AnchorEvent : UnityEvent<ObjectAnchor> { }

		public class PointerHitEvent : UnityEvent<BasePointer> {}

		public class GameObjectEvent : UnityEvent<GameObject> { }

		public class RayNormalEvent : UnityEvent<Vector3, Quaternion> { }

		public class RayFullEvent : UnityEvent<GameObject, Vector3, Quaternion> { }

		public class DirectionEvent : UnityEvent<Direction> {}
		public class PolarityEvent : UnityEvent<Polarity> {}
		public class VanillaSceneEvent : UnityEvent<VanillaScene> { }
		public class VanillaSetupEvent : UnityEvent<SceneSetup> { }
	}

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Global Enums
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	//public enum DebugMode {
	//	All,
	//	LogsOnly,
	//	WarningsOnly,
	//	ErrorsOnly,
	//	RaysOnly,
	//	None
	//}

	[Flags]
	public enum DebugFlags {
		Logs = 1,
		Warnings = 2,
		Errors = 4,
		Rays = 8,
		Gizmos = 16,
		Breaks = 32,
		Misc1 = 64,
		Misc2 = 128,
		Misc3 = 256
	}

	public enum FadeState {
		Clear,
		Faded,
		ManualControl
	}

	public enum SceneType {
		Content,
		Utility,
		System
	}

	public enum LoadAction {
		UnloadIfNotRequired, // If this scene is not included in the next setup, unload it.
		Load, // Load the scene. This is mostly used for loading scenes in the first place, and would rarely be applied to an already active scene.
		Reload, // Adds the Unload instruction immediately followed by the Load instruction, effectively reloading the scene.
		Unload, // Unloads the scene, regardless.
		None // Ignore the scene; keeping it in memory.
	}

	public enum InitState {
		Uninitialized,
		Initializing,
		Initialized,
		InitializationFailed
	}

	public enum Direction {
		Right,
		Left,
		Up,
		Down,
		Forward,
		Back,
		None
	}

	public enum CompassDirection {
		North,
		South,
		East,
		West
	}

	public enum CommonUnityFunction {
		None,
		Awake,
		Start,
		OnEnable,
		OnDisable,
		OnBecameVisible,
		OnBecameInvisible,
		Update,
		FixedUpdate,
		LateUpdate
	}

	public enum DataType {
		Bool,
		Float,
		Int,
		String
	}

	public enum Polarity {
		Positive,
		Neutral,
		Negative
	}

	public enum Axis {
		None,
		X,
		Y,
		Z
	}

	public enum Handedness {
		Middle = 1,
		Left = 2,
		Right = 4
	}

	public enum VanillaModule {
		Project,
		Vanilla,
		Vanilla2D,
		Vanilla3D,
		Vanilla360,
		VanillaVoxels,
		VanillaRelease,
		VanillaVR,
		VanillaAR
	}

	public enum InterpolationType {
		Linear,
		EaseIn,
		EaseOut,
		EaseInAndOut,
		Wobble,
		Bounce
	}

	[Flags]
	public enum TransformMode {
		None,
		Position,
		Rotation,
		Scale
	}

	public enum SceneActivationMode {
		None,
		Immediately,
		AfterLoadingScreen
	}


	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Interfaces
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	//public interface ITransition {
	//	string transitionName();
	//	void Initialize();
	//	IEnumerator Animate(float timeInSeconds = 1.0f); // These -have- to be 'abstract', which means you can't have any base code. 
	//}

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Custom Editor Attributes [this should go in VanillaEditor]
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class ReadOnlyAttribute : PropertyAttribute {}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
	public class ReadOnlyAttributeDrawer : PropertyDrawer {
		public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label) {
			bool wasEnabled = GUI.enabled;
			GUI.enabled = false;
			EditorGUI.PropertyField(rect, prop, true);
			GUI.enabled = wasEnabled;
		}
	}
#endif
}