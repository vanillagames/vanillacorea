﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla.Inputs;
using Vanilla.CustomEvents;

namespace Vanilla.EventSystems {
	
	///------------------------------------------------------------------------------------------------------------------------------------------------------
	/// Enums
	///------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public enum PointerModuleType {
		None,
		Canvas,
		World
	}

	public enum PointerResult {
		NoHit,
		HitEnter,
		HitExit,
		HitSwap,
		HitSame
	}

	///------------------------------------------------------------------------------------------------------------------------------------------------------
	/// Base Class
	///------------------------------------------------------------------------------------------------------------------------------------------------------

	public class BasePointer : VanillaBehaviour {
		[Header("BasePointer")]
		[Header("Setup")]
		//[Tooltip("When should the pointer 'click'?")]
		//public InputEventSubscription clickDownSubscription;

		//[Tooltip("When should the pointer 'unclick'?")]
		//public InputEventSubscription clickUpSubscription;

		[Range(1.0f, 100.0f)]
		public float rayLength = 10.0f;

		private PointerEventData _pointerEventData;
		public PointerEventData pointerEventData {
			get {
				if (_pointerEventData == null) {
					BuildPointerEventData();
				}

				return _pointerEventData;
			}
			set {
				_pointerEventData = value;
			}
		}

		[Header("Status")]
		[ReadOnly]
		public int pointerID;

		private static List<BasePointer> _pointers;
		public static List<BasePointer> pointers {
			get {
				if (_pointers == null) {
					_pointers = new List<BasePointer>();
				}

				return _pointers;
			}
			set {
				_pointers = value;
			}
		}

		[ReadOnly]
		private GameObject _currentHit;
		public GameObject currentHit {
			get {
				return _currentHit;
			}
			set {
				if (_currentHit != value) {
					_currentHit = value;

					HandleNewHit();
				}
			}
		}

		[HideInInspector]
		public GameObject downObject;

		[HideInInspector]
		public LineRenderer line;

		public Transform reticle;

		[ReadOnly]
		public PointerResult pointerStatus;

		[ReadOnly]
		[Tooltip("Where did this pointer terminate in world space?")]
		public Vector3 hitPosition;

		[ReadOnly]
		[Tooltip("What is the rotation of the hit? A pointer that misses defaults to the direction of the ray itself.")]
		public Quaternion hitRotation;

		public GameObjectEvent onHitObjectChanged = new GameObjectEvent();

		//public RayNormalEvent onNewHitPosition = new RayNormalEvent();

		[Tooltip("This event can be used to receive a per-frame update on the ray.")]
		public PointerHitEvent onPointerUpdate = new PointerHitEvent();

		[Tooltip("This event is only invoked if the hit position is ")]
		public RayNormalEvent onHitPositionChanged = new RayNormalEvent();

		[Tooltip("How far away does a hit position need to be from the last in order to invoke the onHitPositionChanged event?")]
		public float hitPositionEventDistanceThreshold = 0.01f;

		public virtual void Awake() {
			if (GetComponent<LineRenderer>()) {
				line = GetComponent<LineRenderer>();
				line.positionCount = 2;
			}
		}

		public virtual void BuildPointerEventData() {
			pointerEventData = new PointerEventData(EventSystem.current);
		}

		public virtual void OnEnable() {
			pointers.Add(this);

			UpdatePointerIDs();

			if (line) {
				onPointerUpdate.AddListener(UpdateLineRenderer);
			}

			if (reticle) {
				onPointerUpdate.AddListener(UpdateReticle);
			}
		}

		public virtual void HandleNewHit() {
			onHitObjectChanged.Invoke(currentHit);
		}

		public static void UpdatePointerIDs() {
			for (int i = 0; i < pointers.Count; i++) {
				pointers[i].pointerID = pointers.IndexOf(pointers[i]);
			}
		}

		public virtual void OnDisable() {
			pointers.Remove(this);

			UpdatePointerIDs();

			if (line) {
				onPointerUpdate.RemoveListener(UpdateLineRenderer);
			}

			if (reticle) {
				onPointerUpdate.RemoveListener(UpdateReticle);
			}
		}

		public void EnterHandler() {
			if (HitIsNotAlreadyEnteredByAnotherPointer()) {
				Log("Pointer entering [{0}]", currentHit);
				ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.pointerEnterHandler);
			}
		}

		public void ExitHandler() {
			if (HitIsNotAlreadyEnteredByAnotherPointer()) {
				Log("Pointer exiting [{0}]", currentHit);

				ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.pointerExitHandler);
			}
		}

		public void DownHandler() {
			if (!currentHit) {
				return;
			}

			Log("Pointer down");

			ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.pointerDownHandler);

			downObject = currentHit;
		}

		public void UpHandler() {
			if (!currentHit) {
				return;
			}

			Log("Pointer up");

			ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.pointerUpHandler);

			// If the localHit is the same as the object we cached in DownHandler, process a click on it as well.
			if (currentHit != null && downObject == currentHit) {
				ClickHandler();
				SubmitHandler();
			}

			downObject = null;
		}

		public void ClickHandler() {
			Log("Pointer click");

			ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.pointerClickHandler);
		}

		public void SubmitHandler() {
			Log("Pointer submit");

			ExecuteEvents.Execute(currentHit, pointerEventData, ExecuteEvents.submitHandler);
		}

		public void UpdateLineRenderer(BasePointer p) {
			if (line) {
				line.SetPosition(0, transform.position);
				line.SetPosition(1, p.hitPosition);
			}
		}

		public void UpdateReticle(BasePointer p) {
			if (reticle) {
				reticle.position = p.hitPosition;
				reticle.rotation = p.hitRotation;
			}
		}

		public virtual bool ModuleHitIsNotEnteredByAnotherOfTheSameModule(PointerModuleType moduleType) {
			return false;
		}

		///// <summary>
		///// This function invokes the onNewHitPosition event which emits two parameters, a worldspace position and a rotation. The worldspace position here factors in UI elements as well as 
		///// </summary>
		///// <param name="newHitPosition"></param>
		///// <param name="newHitRotation"></param>
		//public void UpdateHitPosition(Vector3 newHitPosition, Quaternion newHitRotation) {
		//	hitPosition = newHitPosition;

		//	onNewHitPosition.Invoke(newHitPosition, newHitRotation);
		//}

		public void AssessRayResult(GameObject newHit) {
			// Did we hit something or is it null?
			if (newHit) {
				// Do we have anything already cached?
				if (!currentHit) {
					// We've entered a new object with no prior result cached.
					pointerStatus = PointerResult.HitEnter;

					// Cache the result.
					currentHit = newHit;

					// If the object isn't already considered entered by something else, then 'enter' it.
					EnterHandler();

					// Otherwise, if the ray hit something, we already have a localHit cached and they're different objects...
				} else if (newHit != currentHit) {
					// We've entered a new object with a prior result already cached.
					pointerStatus = PointerResult.HitSwap;

					// Tell the current localHit object that its been 'exited', cache the new localHit object and then handle 'entering' it.
					ExitHandler();

					currentHit = newHit;

					EnterHandler();
				} else {
					pointerStatus = PointerResult.HitSame;
				}
			} else if (currentHit) {
				// We just exited our cached object.
				pointerStatus = PointerResult.HitExit;

				ExitHandler();

				currentHit = null;
			} else {
				// We didn't hit anything and didn't have anything stored either.
				pointerStatus = PointerResult.NoHit;
			}
		}

		public bool HitIsNotAlreadyEnteredByAnotherPointer() {
			for (int i = 0; i < pointers.Count; i++) {
				// We also want to ensure that we're not checking what this pointer has stored, we only want to check the others.
				if (i != pointerID && currentHit == pointers[i].currentHit) {
					return false;
				}
			}

			return true;
		}

		public Vector3 GetRayMissPosition() {
			return transform.position + (transform.forward * rayLength);
		}

		public Quaternion GetRayHitRotation() {
			return Quaternion.Euler(transform.forward + (Vector3.one * 180f));
		}
	}

	///------------------------------------------------------------------------------------------------------------------------------------------------------
	/// Base Module
	///------------------------------------------------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class PointerModule {
		[SerializeField]
		public bool _inUse = true;
		public bool inUse {
			get {
				return _inUse;
			}
			set {
				// If the module is inUse and it gets turned off
				if (_inUse && !value) {
					// We process an 'empty' ray result to flush the selection tracking.
					AssessRayResult(null);
				} else {
					// [ToDo] We could fire a ray right here, so its possible to activate the module and fire a ray in the same frame?
					// But it requires some PointerData, which is inheritance-specific at the moment.
					//FireRay();
				}

				_inUse = value;
			}
		}

		[HideInInspector]
		public BasePointer pointer;

		[HideInInspector]
		public Transform pointerTransform;

		[ReadOnly]
		public PointerModuleType moduleType;

		[Header("Status")]
		[ReadOnly]
		public GameObject currentHit;

		[ReadOnly]
		public Vector3 hitPosition;

		[ReadOnly]
		public float distanceToHit;

		[ReadOnly]
		public PointerResult rayResult;

		public virtual void FireRay(PointerEventData pointerData) {

		}

		public void AssignPointer(BasePointer newPointer) {
			pointer = newPointer;
			pointerTransform = pointer.transform;
			distanceToHit = pointer.rayLength;
		}

		public virtual void OnEnable() {

		}

		public virtual void OnDisable() {
			if (currentHit) {
				if (pointer.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType)) {
					ExitHandler();
				}

				currentHit = null;
			}
		}

		public void GetDistanceToHit() {
			distanceToHit = Vector3.Distance(pointerTransform.position, hitPosition);
		}

		public void AssessRayResult(GameObject newHit) {
			// Did we hit something or is it null?
			if (newHit) {
				// Do we have anything already cached?
				if (!currentHit) {
					// We've entered a new object with no prior result cached.
					rayResult = PointerResult.HitEnter;

					// Cache the result.
					currentHit = newHit;

					// If the object isn't already considered entered by something else, then 'enter' it.
					if (pointer.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType)) {
						EnterHandler();
					}

					// Otherwise, if the ray hit something, we already have a localHit cached and they're different objects...
				} else if (newHit != currentHit) {
					// We've entered a new object with a prior result already cached.
					rayResult = PointerResult.HitSwap;

					// Tell the current localHit object that its been 'exited', cache the new localHit object and then handle 'entering' it.
					if (pointer.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType)) {
						ExitHandler();
					}

					currentHit = newHit;

					if (pointer.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType)) {
						EnterHandler();
					}
				} else {
					rayResult = PointerResult.HitSame;
				}
			} else if (currentHit) {
				// We just exited our cached object.
				rayResult = PointerResult.HitExit;

				if (pointer.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType)) {
					ExitHandler();
				}

				currentHit = null;
			} else {
				// We didn't hit anything and didn't have anything stored either.
				rayResult = PointerResult.NoHit;
			}
		}

		public virtual void EnterHandler() {
			// We don't want the modules to handle entering and exiting anymore; that'll be done by the Pointer itself.
			// The reason for this is that we need to compare which hit is closest out of the two modules in order to
			// give the illusion of one unified line-of-sight.

			//ExecuteEvents.Execute(currentHit, pointer.pointerEventData, ExecuteEvents.pointerEnterHandler);

			//EventSystem.current.SetSelectedGameObject(currentHit);
		}

		public virtual void ExitHandler() {
			//ExecuteEvents.Execute(currentHit, pointer.pointerEventData, ExecuteEvents.pointerExitHandler);

			//EventSystem.current.SetSelectedGameObject(null);
		}

		public virtual void PopulateRayResults(BasePointer p) {

		}
	}

	///----------------------------------------------------------------------------------------------------------------------------
	/// Canvas Pointer
	///----------------------------------------------------------------------------------------------------------------------------
	
	[Serializable]
	public class CanvasPointerModule : PointerModule {

		public static Canvas canvas;

		[Tooltip("A camera is required to raycast UI objects that are part of Unitys EventSystem.")]
		[ReadOnly]
		public Camera eventCamera;

		List<RaycastResult> results = new List<RaycastResult>();

		[ReadOnly]
		public RectTransform hitRect;

		public void Awake() {
			moduleType = PointerModuleType.Canvas;
		}

		public override void FireRay(PointerEventData pointerData) {
			if (!canvas || !inUse) {
				return;
			}

			canvas.worldCamera = eventCamera;

			// This works in an interesting way. As far as I can tell, it iterates through each raycast-target UI element in order of the hierarchy (!) which might mean we need to do our own proximity/depth checking.
			EventSystem.current.RaycastAll(pointerData, results);

			// If the results returned anything
			if (results.Count > 0) {
				// See what we should do with it.
				AssessRayResult(results[0].gameObject);
			} else {
				// We use the same function to handle 'nothing' and empty our cached hit results.
				AssessRayResult(null);
			}

			// The way UI element results work are a bit different to normal raycast results, so we have to handle them seperately.
			switch (rayResult) {
				case PointerResult.NoHit:

					break;
				case PointerResult.HitEnter:
					hitRect = currentHit.transform as RectTransform;
					break;
				case PointerResult.HitExit:
					hitRect = null;
					break;
				case PointerResult.HitSwap:
					hitRect = currentHit.transform as RectTransform;
					break;
				case PointerResult.HitSame:

					break;
			}

			if (hitRect) {
				// This is Unity's magic feature for getting a world space position of an EventSystem raycast hit.
				RectTransformUtility.ScreenPointToWorldPointInRectangle(hitRect, pointer.pointerEventData.position, eventCamera, out hitPosition);

				GetDistanceToHit();
			} else {
				distanceToHit = pointer.rayLength;
			}
		}

		public override void EnterHandler() {
			base.EnterHandler();

			//pointer.Log("My canvas pointer just entered [{0}]", currentHit);
		}

		public override void ExitHandler() {
			base.ExitHandler();

			//pointer.Log("My canvas pointer just exited [{0}]", currentHit);
		}

		public override void PopulateRayResults(BasePointer p) {
			p.hitPosition = hitPosition;
			p.hitRotation = hitRect.rotation;

			p.AssessRayResult(currentHit);
		}
	}

	///----------------------------------------------------------------------------------------------------------------------------
	/// Collider / PhysX Pointer [Single]
	///----------------------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class WorldPointerModule : PointerModule {
		public RaycastHit hit;

		[SerializeField]
		public LayerMask layerMask;

		// This is how you reset a default value for a subclass, except it doesn't work!
		public void Reset() {
			moduleType = PointerModuleType.World;
		}

		public void Awake() {
			moduleType = PointerModuleType.World;
		}

		public override void FireRay(PointerEventData pointerData) {
			if (!inUse || !pointerTransform) {
				return;
			}

			// If we're in pierce mode... 
			Physics.Raycast(pointerTransform.position, pointerTransform.forward, out hit, pointer.rayLength, layerMask);

			if (hit.collider) {
				AssessRayResult(hit.collider.gameObject);

				hitPosition = hit.point;

				distanceToHit = hit.distance;
				//GetDistanceToHit();
			} else {
				AssessRayResult(null);

				distanceToHit = pointer.rayLength;
			}
		}

		public override void EnterHandler() {
			base.EnterHandler();

			//pointer.Log("My world pointer just entered [{0}]", currentHit);
		}

		public override void ExitHandler() {
			base.ExitHandler();

			//pointer.Log("My world pointer just exited [{0}]", currentHit);
		}

		public override void PopulateRayResults(BasePointer p) {
			p.hitPosition = hit.point;
			p.hitRotation = Quaternion.LookRotation(hit.normal);

			p.AssessRayResult(currentHit);
		}
	}

	///----------------------------------------------------------------------------------------------------------------------------
	/// Collider / PhysX Pointer [All]
	///----------------------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class WorldPointerAllModule : PointerModule {

		[SerializeField]
		public LayerMask layerMask;

		[Tooltip("Since we're using non-allocation in the raycast results, we have to nominate a specific maximum number of results to cache.")]
		int maximumNumberOfResults = 5;

		RaycastHit[] nonAllocResults;

		[ReadOnly]
		public List<Collider> trackedObjects = new List<Collider>();

		void Awake() {
			nonAllocResults = new RaycastHit[maximumNumberOfResults];
		}

		public override void FireRay(PointerEventData pointerData) {
			if (!inUse || !pointerTransform) {
				return;
			}

			Physics.RaycastNonAlloc(pointerTransform.position, pointerTransform.forward, nonAllocResults, pointer.rayLength, layerMask);

			// Cycle through all the results from this frame and check if the tracked list doesn't contain it
			for (int i = 0; i < maximumNumberOfResults; i++) {
				if (!trackedObjects.Contains(nonAllocResults[i].collider)) {
					// If it isnt there, its new and we should add it
					ColliderEntered(nonAllocResults[i].collider);
				}
			}

			// Now we check for any tracked objects that are no longer hit by the ray
			for (int i = 0; i < trackedObjects.Count; i++) {
				bool trackedObjectIsStillHit = false;

				// Cycle through all the tracked objects and check if they match one from the array for this frame.
				// If it doesn't match any, it means its no longer 'seen' by the ray and we should 'exit' it.
				for (int r = 0; r < maximumNumberOfResults; r++) {
					if (trackedObjects[i] == nonAllocResults[r].collider) {
						trackedObjectIsStillHit = true;
						r = maximumNumberOfResults;
					}
				}

				if (!trackedObjectIsStillHit) {
					ColliderExited(trackedObjects[i]);
				}
			}

			//float shortestDistance = Mathf.Infinity;
			//int nearestID = -1;

			//// Cycle through the results and check the distance of each.
			//// Whichever, if any, is the shortest is then processed as the 'current' result from this module
			//for (int i = 0; i < maximumNumberOfResults; i++) {
			//	if (nonAllocResults[i].collider != null) {
			//		if (nonAllocResults[i].distance < shortestDistance) {
			//			shortestDistance = nonAllocResults[i].distance;
			//			nearestID = i;
			//		}
			//	}
			//}

			//if (nearestID > 0) {
			//	AssessRayResult(nonAllocResults[nearestID].collider.gameObject);

			//	hitPosition = nonAllocResults[nearestID].point;

			//	distanceToHit = nonAllocResults[nearestID].distance;
			//} else {
			//	AssessRayResult(null);

			//	distanceToHit = pointer.rayLength;
			//}
		}

		public void ColliderEntered(Collider c) {
			trackedObjects.Add(c);
		}

		public void ColliderExited(Collider c) {
			trackedObjects.Remove(c);
		}
	}
}