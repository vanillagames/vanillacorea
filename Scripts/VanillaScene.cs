﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using System.IO;
using System.Collections;
using System.Collections.Generic;

using Vanilla.Transitions;

namespace Vanilla {
#if UNITY_EDITOR
	using UnityEditor;

	[CanEditMultipleObjects]
	[CustomEditor(typeof(VanillaScene), true)]
	public class VanillaSceneEditor : VanillaEditor {

		VanillaScene scene;

		[Tooltip("Which Vanilla submodule folder should this scene asset be created in if the 'Create SceneAsset' button is clicked?")]

		VanillaModule assetCreationModule = VanillaModule.Project;

		public void OnEnable() {
			scene = (VanillaScene)target;

			OptimizeOverride();
			UpdateSceneAssetBuildIndex();
			SaveSceneAssetFile();
		}

		/// <summary>
		/// Because the setup loading process contains a lot of potential coroutine calls, it would be helpful to know which steps (if any) could theoretically be skipped.
		/// In fact, thats exactly what we can do by checking if the declaring type of each specific method matches that of the base class (this one!). If it doesn't, it must be overridden.
		/// If we keep track of those bools, the SceneLoader can use that information at run-time to know what processes to bypass. Handy! But is this really just something the compiler might already do for us..?
		/// </summary>
		public void OptimizeOverride() {
			if (!scene.asset) {
				VanillaLog.Log("Please assign or create a Scene Asset file! [{0}]", scene.gameObject.scene.name);
				return;
			}

			System.Type classType = scene.GetType();
			
			scene.asset.sceneLoadedIsOverridden = classType.GetMethod("SceneLoaded").DeclaringType != typeof(VanillaScene);
			scene.asset.sceneUnloadIsOverridden = classType.GetMethod("SceneUnloaded").DeclaringType != typeof(VanillaScene);
			scene.asset.setupLoadedIsOverridden = classType.GetMethod("SetupLoaded").DeclaringType != typeof(VanillaScene);
			scene.asset.setupUnloadedIsOverridden = classType.GetMethod("SetupUnloaded").DeclaringType != typeof(VanillaScene);
			scene.asset.activateIsOverridden = classType.GetMethod("Activate").DeclaringType != typeof(VanillaScene);
			scene.asset.deactivateIsOverridden = classType.GetMethod("Deactivate").DeclaringType != typeof(VanillaScene);
		}

		/// <summary>
		/// This is one of those 'Unity-won't-do-it-so-we'll-do-it-ourselves' things.
		/// Refering a Unity scene file in a VanillaSceneAsset file is all well and good for the editor but in true Unity style, this is completely unusable for builds.
		/// Not only this, the only callback for preprocessing at build-time has been made obsolete and there's no replacement at time of writing.
		/// So what we'll do is ensure that, when a VanillaScene is opened in the editor, it secretly populates a string/index int field in the VanillaSceneAsset -based- on the Unity Scene file
		/// and SceneLoader will use that instead. Technically speaking, it seems that a string or build index int is the only viable method for loading a scene in a build ¯\_(ツ)_/¯ int is likely faster.
		/// </summary>
		public void UpdateSceneAssetBuildIndex() {
			if (scene.asset) {
				//scene.asset.loadString = scene.asset.name;
				scene.asset.buildID = SceneManager.GetSceneByName(scene.asset.scene.name).buildIndex;
			}
		}

		public void SaveSceneAssetFile() {
			if (scene.asset) {
				EditorUtility.SetDirty(scene.asset);
				AssetDatabase.SaveAssets();
			}
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			assetCreationModule = (VanillaModule)EditorGUILayout.EnumPopup("New SceneAsset Location", assetCreationModule);

			if (!Application.isPlaying && GUILayout.Button("Create SceneAsset")) {
				string assetPath = GetModulePath(assetCreationModule);

				if (!AssetDatabase.IsValidFolder(assetPath+ "/SceneAssets")) {
					AssetDatabase.CreateFolder(assetPath, "SceneAssets");
				}

				assetPath += "/SceneAssets/["+ assetCreationModule.ToString() + "] " + behaviour.gameObject.scene.name + ".asset";

				VanillaSceneAsset sceneAsset = null;

				if (File.Exists(assetPath)) {
					VanillaLog.Log("There is already an asset at the following path [{0}]", assetPath);

					sceneAsset = (VanillaSceneAsset)AssetDatabase.LoadAssetAtPath(assetPath, typeof(VanillaSceneAsset));

					scene.asset = sceneAsset;
				} else {
					sceneAsset = CreateInstance<VanillaSceneAsset>();

					sceneAsset.scene = AssetDatabase.LoadAssetAtPath(scene.gameObject.scene.path, typeof(Object));
					scene.asset = sceneAsset;

					AssetDatabase.CreateAsset(sceneAsset, assetPath);
				}

				EditorUtility.SetDirty(scene);

				AssetDatabase.Refresh();

				UpdateSceneAssetBuildIndex();
				SaveSceneAssetFile();

				AssetDatabase.SaveAssets();

				Selection.activeObject = sceneAsset;

				EditorUtility.FocusProjectWindow();
			}

			 
		}
	}
#endif

	public class VanillaScene : VanillaBehaviour {

		[Header("[ Vanilla Scene ]")]

		[Tooltip("What should the scene be addressed as in the scene dictionary? If left blank, the name of the scene itself will be used.")]
		public VanillaSceneAsset asset;

		[Tooltip("When the next setup is loaded, what should we do with this scene? See the LoadAction enum definition for more details on each state.")]
		public LoadAction actionAtNextSceneSetup;

		public SceneActivationMode activationMode;

		//[Header("Magic auto-optimized don't-run-that-coroutine flags")]
		//[HideInInspector]
		//[SerializeField]
		//[ReadOnly]
		//public bool sceneLoadedIsOverridden;
		//[SerializeField]
		//[ReadOnly]
		//public bool sceneUnloadIsOverridden;
		//[SerializeField]
		//[ReadOnly]
		//public bool setupLoadedIsOverridden;
		//[SerializeField]
		//[ReadOnly]
		//public bool setupUnloadedIsOverridden;
		//[SerializeField]
		//[ReadOnly]
		//public bool activateIsOverridden;
		//[SerializeField]
		//[ReadOnly]
		//public bool deactivateIsOverridden;

		//[HideInInspector]
		public UnityEvent onSetupLoaded = new UnityEvent();
		//[HideInInspector]
		public UnityEvent onSetupUnloaded = new UnityEvent();
		//[HideInInspector]
		public UnityEvent onActivation = new UnityEvent();
		//[HideInInspector]
		public UnityEvent onDeactivation = new UnityEvent();

		public virtual void Awake() {
			switch (activationMode) {
				default:
					if (VanillaSceneLoader.i) {
						VanillaSceneLoader.i.activeVanillaScene = this;
					} else {
						Warning("The Vanilla Scene Loader is currently unavailable.");
					}
					break;
				case SceneActivationMode.None:
					break;
			}
		}

		public virtual void OnEnable() {
			if (!VanillaSceneLoader.i) {
				Warning("The SceneLoader singleton is not currently available.");
				return;
			}

			asset.loadedScene = this;

			VanillaSceneLoader.i.currentlyLoadedScenes.Add(this);
		}

		public void OnDisable() {
			if (!VanillaSceneLoader.i) {
				if (Vanilla.applicationQuitBegun) {
					return;
				} else {
					Warning("The SceneLoader is not currently unavailable.");
					return;
				}
			}

			asset.loadedScene = null;

			VanillaSceneLoader.i.currentlyLoadedScenes.Remove(this);
		}

		/// <summary>
		/// This runs the moment the scene is loaded into memory. Other scenes in the Setup are not guaranteed to be in memory at this point, so functionality should be limited to objects and scripts local to this particular scene.
		/// </summary>
		public virtual IEnumerator SceneLoaded() {
			yield return null;
		}

		/// <summary>
		/// This runs the moment the scene is loaded into memory. Other scenes in the Setup are not guaranteed to be in memory at this point, so functionality should be limited to objects and scripts local to this particular scene.
		/// </summary>
		public virtual void SceneLoadedVoid() {}

		/// <summary>
		/// This occurs just before the scene is unloaded and is technically the last automatic call received from VanillaSceneLoader.
		/// </summary>
		public virtual IEnumerator SceneUnloaded() {
			yield return null;
		}

		public virtual void SceneUnloadedVoid() { }

		/// <summary>
		/// This is run once all scenes in a Setup have been processed and loaded. No new scenes will be loaded or unloaded at this point unless otherwise specified.
		/// </summary>
		public virtual IEnumerator SetupLoaded() {
			yield return null;
		}

		public virtual void SetupLoadedVoid() { }

		/// <summary>
		///	This is run just before we process the next Setup and LoadQueue. All current scenes will still be in loaded at this point.
		/// </summary>
		public virtual IEnumerator SetupUnloaded() {
			yield return null;
		}

		public virtual void SetupUnloadedVoid() { }
		
		/// <summary>
		/// This is the last thing the SceneLoader will invoke automatically, and should be considered the 'starting pistol' of a Vanilla setup. It is only run for one VanillaScene, the one nominated for activation in the Setup asset file.
		/// </summary>
		public virtual IEnumerator Activate() {
			yield return null;
		}

		public virtual void ActivateVoid() { }

		/// <summary>
		/// This is run on the active VanillaScene just before a different scene is activated.
		/// </summary>
		public virtual IEnumerator Deactivate() {
			yield return null;
		}

		public virtual void DeactivateVoid() { }
	}
}