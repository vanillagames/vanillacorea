﻿using UnityEngine;
using UnityEngine.UI;

using System;

using Vanilla;
using Vanilla.Inputs;

public class AxisFloatSubscriptionExample : VanillaBehaviour {

	private Text text;

	public string axisName = "1";
	public int inputID = 0;

	private float valueDelta;

	void Awake() {
		text = GetComponent<Text>();
	}

	public void OnEnable() {
		if (VanillaInputs.i) {
			//VanillaInputs.i.SubscribeToAxisFloat(axisName, inputID, AxisValueChanged);
		}
	}

	public void OnDisable() {
		if (VanillaInputs.i) {
			//VanillaInputs.i.UnsubscribeFromAxisFloat(axisName, inputID, AxisValueChanged);
		}
	}

	public void AxisValueChanged(float newValue) {
		valueDelta = Mathf.Clamp(valueDelta + newValue, -100, 100);

		text.text = "Chance of explosions [ " + valueDelta + "% ]";
	}
}