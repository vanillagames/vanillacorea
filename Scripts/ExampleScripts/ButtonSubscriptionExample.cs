﻿using UnityEngine;
using UnityEngine.UI;

using System;

using Vanilla;
using Vanilla.Inputs;

public class ButtonSubscriptionExample : VanillaBehaviour {

	private Text text;

	public string buttonName = "Button 1";
	public int inputID = 0;

	void Awake() {
		text = GetComponent<Text>();
	}

	public void OnEnable() {
		if (VanillaInputs.i) {
			//VanillaInputs.i.SubcribeToButton(buttonName, inputID, ButtonState.Pressed, ButtonPushed);
		}
	}


	public void OnDisable() {
		//VanillaInputs.i.UnsubscribeFromButton(buttonName, inputID, ButtonState.Pressed, ButtonPushed);
	}

	public void ButtonPushed() {
		switch (UnityEngine.Random.Range(0, 4)) {
			case 0:
				text.text = "Hey ";
				break;
			case 1:
				text.text += "Ho ";
				break;
			case 2:
				text.text += "Let's ";
				break;
			case 3:
				text.text += "Go! ";
				break;
		}
	}
}