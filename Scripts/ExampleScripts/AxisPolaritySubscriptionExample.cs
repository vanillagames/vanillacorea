﻿using UnityEngine;
using UnityEngine.UI;

using System;

using Vanilla;
using Vanilla.Inputs;

public class AxisPolaritySubscriptionExample : VanillaBehaviour {

	private Text text;

	public string axisName = "1";
	public int inputID = 0;


	void Awake() {
		text = GetComponent<Text>();
	}

	public void OnEnable() {
		if (VanillaInputs.i) {
			//VanillaInputs.i.SubscribeToAxisPolarity(axisName, inputID, Polarity.Positive, AxisPolarityChangedToPositive);
			//VanillaInputs.i.SubscribeToAxisPolarity(axisName, inputID, Polarity.Neutral, AxisPolarityChangedToNeutral);
			//VanillaInputs.i.SubscribeToAxisPolarity(axisName, inputID, Polarity.Negative, AxisPolarityChangedToNegative);
		}
	}

	public void OnDisable() {
		if (VanillaInputs.i) {
			//VanillaInputs.i.UnsubscribeFromAxisPolarity(axisName, inputID, Polarity.Positive, AxisPolarityChangedToPositive);
			//VanillaInputs.i.UnsubscribeFromAxisPolarity(axisName, inputID, Polarity.Neutral, AxisPolarityChangedToNeutral);
			//VanillaInputs.i.UnsubscribeFromAxisPolarity(axisName, inputID, Polarity.Negative, AxisPolarityChangedToNegative);
		}
	}

	public void AxisPolarityChangedToPositive() {
		text.text = "Jeepers creepers yourself, buddy!";
	}

	public void AxisPolarityChangedToNeutral() {
		text.text = "That's more like it.";
	}

	public void AxisPolarityChangedToNegative() {
		text.text = "That's low, even for me.";
	}
}