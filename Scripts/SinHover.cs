﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla;

public class SinHover : MonoBehaviour {

	public float amount = 1.0f;
	public float speed = 1.0f;

	float offset;

	//float t;

	public Axis axis = Axis.Y;

	void Awake() {
		offset = Random.value;
	}

	void Update() {
		//t = Mathf.Repeat(t + (Time.deltaTime * speed), float.MaxValue);

		float sin = Mathf.Sin((Time.time * speed) + offset) * amount;

		switch (axis) {
			case Axis.X:
				transform.localPosition = Vector3.right * sin;
				break;
			case Axis.Y:
				transform.localPosition = Vector3.up * sin;
				break;
			case Axis.Z:
				transform.localPosition = Vector3.forward * sin;
				break;
		}
	}
}
