﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using System;
using System.Collections.Generic;

namespace Vanilla.EventSystems {
	public class PointerTarget : VanillaBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler {

		[EnumFlags]
		public Handedness pointerFilter;

		public UnityEvent onPointerEnter = new UnityEvent();
		public UnityEvent onPointerExit = new UnityEvent();

		public UnityEvent onPointerDown = new UnityEvent();
		public UnityEvent onPointerUp = new UnityEvent();

		public UnityEvent onPointerClick = new UnityEvent();

		public void OnPointerEnter(PointerEventData e) {
			if (ButtonIsAllowed(e.button)) {
				onPointerEnter.Invoke();
			}
		}

		public void OnPointerExit(PointerEventData e) {
			if (ButtonIsAllowed(e.button)) {
				onPointerExit.Invoke();
			}
		}

		public void OnPointerDown(PointerEventData e) {
			if (ButtonIsAllowed(e.button)) {
				onPointerDown.Invoke();
			}
		}

		public void OnPointerUp(PointerEventData e) {
			if (ButtonIsAllowed(e.button)) {
				onPointerUp.Invoke();
			}
		}

		public void OnPointerClick(PointerEventData e) {
			if (ButtonIsAllowed(e.button)) {
				onPointerClick.Invoke();
			}
		}

		bool ButtonIsAllowed(PointerEventData.InputButton button) {
			switch (button) {
				case PointerEventData.InputButton.Left:
					return pointerFilter.HasFlag(Handedness.Left);
				case PointerEventData.InputButton.Middle:
					return pointerFilter.HasFlag(Handedness.Middle);
				case PointerEventData.InputButton.Right:
					return pointerFilter.HasFlag(Handedness.Right);
				default:
					return false;
			}
		}
	}
}