﻿using UnityEngine;

using Vanilla.EventSystems;

namespace Vanilla {
	public class WorldAndCanvasPointer : BasePointer {
		[Header("[ World + Canvas Pointer ]")]
		[ReadOnly]
		private PointerModuleType _nearestHit;
		public PointerModuleType nearestHit {
			get {
				return _nearestHit;
			}
			set {
				_nearestHit = value;

				switch (_nearestHit) {
					case PointerModuleType.None:
						BothModulesMissed();
						break;
					case PointerModuleType.Canvas:
						canvasPointer.PopulateRayResults(this);
						break;
					case PointerModuleType.World:
						worldPointer.PopulateRayResults(this);
						break;
				}
			}
		}

		[Header("Setup")]
		[SerializeField]
		public CanvasPointerModule canvasPointer;

		[SerializeField]
		public WorldPointerModule worldPointer;

		public override void Awake() {
			base.Awake();

			canvasPointer.AssignPointer(this);
			worldPointer.AssignPointer(this);

			if (!canvasPointer.eventCamera) {
				if (GetComponent<Camera>()) {
					canvasPointer.eventCamera = GetComponent<Camera>();
				} else {
					Error("No Camera component is attached to this GameObject, which the CanvasPointer requires for use.");
				}
			}
		}

		public override void BuildPointerEventData() {
			base.BuildPointerEventData();

			pointerEventData.position = new Vector2(canvasPointer.eventCamera.pixelWidth * 0.5f, canvasPointer.eventCamera.pixelHeight * 0.5f);
		}

		public override void OnEnable() {
			base.OnEnable();
		}

		void Update() {
			// Fire both pointer modules
			canvasPointer.FireRay(pointerEventData);
			worldPointer.FireRay(pointerEventData);

			if (!canvasPointer.currentHit) {
				if (!worldPointer.currentHit) {
					// Both rays missed
					nearestHit = PointerModuleType.None;
				} else {
					// Canvas ray missed, world pointer hit
					nearestHit = PointerModuleType.World;
				}
			} else if (!worldPointer.currentHit) {
				// Canvas ray hit, world pointer missed
				nearestHit = PointerModuleType.Canvas;
			} else {
				// Both rays hit
				// But who was closer..?
				nearestHit = canvasPointer.distanceToHit < worldPointer.distanceToHit ? PointerModuleType.Canvas : PointerModuleType.World;
			}

			// Whatever the outcome, let the people know.
			onPointerUpdate.Invoke(this);
		}

		public override void HandleNewHit() {
			base.HandleNewHit();
		}

		public override void OnDisable() {
			base.OnDisable();
		}

		public override bool ModuleHitIsNotEnteredByAnotherOfTheSameModule(PointerModuleType moduleType) {
			switch (moduleType) {
				case PointerModuleType.Canvas:
					for (int i = 0; i < pointers.Count; i++) {
						// We also want to ensure that we're not checking what this pointer has stored, we only want to check the others.
						if (i != pointerID && canvasPointer.currentHit == ((WorldAndCanvasPointer)pointers[i]).canvasPointer.currentHit) {
							return false;
						}
					}

					break;
				case PointerModuleType.World:
					for (int i = 0; i < pointers.Count; i++) {
						if (i != pointerID && worldPointer.currentHit == ((WorldAndCanvasPointer)pointers[i]).worldPointer.currentHit) {
							return false;
						}
					}

					break;
			}

			return true;
		}

		void BothModulesMissed() {
			hitPosition = GetRayMissPosition();
			hitRotation = GetRayHitRotation();

			AssessRayResult(null);
		}
	}
}