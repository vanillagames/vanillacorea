﻿using UnityEngine;
using System.Collections;

public class TestCoroutineGC : MonoBehaviour {

	public enum TestGCMode {
		Cached,
		NewCoroutines
	}

	public TestGCMode testGCMode;

	public IEnumerator cachedCoroutine;

	public int testPhase = 0;
	public int frame = 0;
	public int frameMax = 100;

	void OnEnable() {
		StartCoroutine(RunTests());
	}

	IEnumerator RunTests() {
		testPhase = 0;

		switch (testGCMode) {
			case TestGCMode.Cached:
				for (int i = 0; i < 5; i++) {
					cachedCoroutine = Test();

					StartCoroutine(cachedCoroutine);

					//cachedCoroutine.
					while (cachedCoroutine.MoveNext()) {
						yield return null;
					}
				}
				break;
			case TestGCMode.NewCoroutines:
				for (int i = 0; i < 5; i++) {
					IEnumerator process = Test();

					StartCoroutine(process);

					while (process.MoveNext()) {
						yield return null;
					}
				}
				break;
		}
	}

	IEnumerator Test() {
		testPhase++;
		frame = 0;

		for (int i = 0; i < frameMax; i++) {
			frame++;
			yield return null;
		}
	}
}
