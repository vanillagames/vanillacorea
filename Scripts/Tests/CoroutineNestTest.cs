﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla;

public class CoroutineNestTest : VanillaBehaviour {

	void OnEnable() {
		StartCoroutine(Process1());
	}

	IEnumerator Process1() {
		yield return null;

		Log("Process 1 - About to start Process 2");
		
		yield return Process2(); // We don't actually have to say 'StartCoroutine' ..? Does this save garbage? If it does...

		Log("Process 1 - Just finished Process 2?");

		// My question then is... since there's no yield break on this line, will it wait until Process2 is done before running Process3?

		Log("Process 1 - About to start Process 3");

		yield return Process3();

		Log("Process 1 - Just finished Process 3?");

		yield return null;
	}

	IEnumerator Process2() {
		Log("Process 2 Begun!");

		int i = 0;

		while (i < 100) {
			i++;
			yield return null;
		}

		Log("Process 2 Finished!");
	}

	IEnumerator Process3() {
		Log("Process 3 Begun!");

		yield return null;

		yield return null;

		Log("Process 3 Finished!");
	}
}