﻿//using UnityEngine;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//using Vanilla;

//public class NestedCoroutineBreakTest : VanillaBehaviour {

//	void OnEnable() {
//		StartCoroutine(one());
//	}

//	IEnumerator one() {
//		Log("one start");

//		yield return two();

//		Log("one end"); // This prints! In other words, nested coroutines handle break locally and their parent coroutine will proceed afterwards.
//	}

//	IEnumerator two() {
//		Log("two start");

//		yield break;

//		Log("two end");
//	}
//}