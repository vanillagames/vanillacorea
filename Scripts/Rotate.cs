﻿using UnityEngine;
using System.Collections;

namespace Vanilla {
	public class Rotate : VanillaBehaviour {

		public Axis axis;
		public Space locality;

		public float rate = 1.0f;

		void Update() {
			switch (axis) {
				case Axis.X:
					transform.Rotate(Vector3.right, rate, locality);
					break;
				case Axis.Y:
					transform.Rotate(Vector3.up, rate, locality);
					break;
				case Axis.Z:
					transform.Rotate(Vector3.forward, rate, locality);
					break;
			}
		}
	}
}