﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Vanilla {
	public class VanillaPreInit : VanillaBehaviour {

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		static void VanillaPreInitialize() {
			// Trying to load a Scene now crashes Unity :( 
		}
	}
}