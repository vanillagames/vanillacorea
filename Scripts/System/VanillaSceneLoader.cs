﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla.CustomEvents;

namespace Vanilla {
	public class VanillaSceneLoader : VanillaSystemModule<VanillaSceneLoader> {

		[Header("[ Scene Loader ]")]
		[Tooltip("If true, VanillaSceneLoader will automatically load the setup in initialSetup once system-wide initialization is complete.")]
		public bool runAutomatically = true;

		[ReadOnly]
		public bool setupIsLoading;

		[Tooltip("If ticked, any concurrent load processes can be interrupted by another of the same kind.")]
		public bool loadCanBeInterrupted = false;

		[Tooltip("If nominated to run automatically, which setup should we load initially?")]
		public SceneSetup initialSetup;

		[Tooltip("Which setup is currently loaded?")]
		[ReadOnly]
		public SceneSetup currentSetup;

		[Tooltip("Which scene is currently active?")]
		[ReadOnly]
		public VanillaScene activeVanillaScene;

		[ReadOnly]
		public List<VanillaScene> currentlyLoadedScenes;

		//public Dictionary<VanillaSceneAsset, VanillaScene> currentlyLoadedSceneDictionary = new Dictionary<VanillaSceneAsset, VanillaScene>();

		IEnumerator loadProcess;

		public StringEvent onNewSceneLoadingDescription = new StringEvent();

		[HideInInspector]
		public VanillaSceneEvent onSceneLoaded = new VanillaSceneEvent();
		public VanillaSceneEvent onSceneUnloaded = new VanillaSceneEvent();
		public VanillaSceneEvent onSceneActivated = new VanillaSceneEvent();
		public VanillaSceneEvent onSceneDeactivated = new VanillaSceneEvent();

		public VanillaSetupEvent onSetupLoaded = new VanillaSetupEvent();
		public VanillaSetupEvent onSetupUnloaded = new VanillaSetupEvent();

		Dictionary<VanillaSceneAsset, LoadAction> loadQueue = new Dictionary<VanillaSceneAsset, LoadAction>();

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Initialization
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public override void SystemInitializationComplete() {
			if (initialSetup && runAutomatically) {
				LoadSetup(initialSetup);
			}
		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Adding Instructions
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public void AddInstructionToLoadQueue(VanillaSceneAsset asset, LoadAction actionToTake) {
			Log("Adding [{0}] instruction for the scene [{1}] to the LoadQueue", actionToTake, asset);

			loadQueue.Add(asset, actionToTake);
		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Load Setup
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public void LoadSetup(SceneSetup incomingSetup) {
			if (!incomingSetup) {
				Error("The given setup is null.");
				return;
			}

			if (loadProcess != null) {
				if (loadCanBeInterrupted) {
					StopCoroutine(loadProcess);
				} else {
					return;
				}
			}

			loadProcess = LoadSetupCoroutine(incomingSetup);

			StartCoroutine(loadProcess);
		}

		public IEnumerator LoadSetupCoroutine(SceneSetup incomingSetup) {
			setupIsLoading = true;

			/// Invoke the 'begun' event. Things in the System scene that 
			onSetupUnloaded.Invoke(currentSetup);

			/// Begin the loading screen
			LoadingScreen currentLoadingScreen = VanillaLoadingScreens.i.GetLoadingScreen(incomingSetup.loadingScreenID);

			if (currentLoadingScreen) {
				currentLoadingScreen.gameObject.SetActive(true);

				if (currentLoadingScreen.beginIsOverridden) {
					yield return currentLoadingScreen.Begin();
				} else {
					currentLoadingScreen.BeginVoid();
				}
			}

			/// Deactivate [Outgoing]
			if (activeVanillaScene) {
				if (activeVanillaScene.asset.deactivateIsOverridden) {
					yield return activeVanillaScene.Deactivate();
				} else {
					activeVanillaScene.DeactivateVoid();
				}

				onSceneDeactivated.Invoke(activeVanillaScene);

				//activeVanillaScene = null;
			}

			/// Setup Unloaded [Outgoing]
			for (int i = 0; i < currentlyLoadedScenes.Count; i++) {
				currentlyLoadedScenes[i].onSetupLoaded.Invoke();

				if (currentlyLoadedScenes[i] != null) {
					if (currentlyLoadedScenes[i].asset.setupUnloadedIsOverridden) {
						yield return currentlyLoadedScenes[i].SetupUnloaded();
					} else {
						currentlyLoadedScenes[i].SetupUnloadedVoid();
					}
				}
			}

			/// Populate the LoadQueue [Outgoing]
			for (int i = 0; i < currentlyLoadedScenes.Count; i++) {
				switch (currentlyLoadedScenes[i].actionAtNextSceneSetup) {
					case LoadAction.None:
						// Ignore this scene; it will stay loaded.
						break;
					case LoadAction.UnloadIfNotRequired:
						bool found = false;

						for (int s = 0; s < incomingSetup.sceneAssets.Length; s++) {
							if (currentlyLoadedScenes[i].asset == incomingSetup.sceneAssets[s]) {
								found = true;
								s = 10000; // Stupid/cheap way to end the for loop
							}
						}

						if (!found) {
							loadQueue.Add(currentlyLoadedScenes[i].asset, LoadAction.Unload);
						}
						break;
					case LoadAction.Reload:
						loadQueue.Add(currentlyLoadedScenes[i].asset, LoadAction.Unload);
						loadQueue.Add(currentlyLoadedScenes[i].asset, LoadAction.Load);
						break;
					default:
						loadQueue.Add(currentlyLoadedScenes[i].asset, currentlyLoadedScenes[i].actionAtNextSceneSetup);
						break;
				}
			}

			/// Populate the LoadQueue [Incoming]
			for (int i = 0; i < incomingSetup.sceneAssets.Length; i++) {
				if (incomingSetup.sceneAssets[i] != null) {
					/// This is the hard part.
					/// When choosing whether or not to load one of the incoming scenes, several considerations need to be made first in order to prevent unwanted double-loading.

				bool sceneIsCurrentlyLoaded = incomingSetup.sceneAssets[i].loadedScene != null;

				if /// We only want to load the scene if...
					(!sceneIsCurrentlyLoaded /// It isn't already loaded...
				|| /// ...or...
					(sceneIsCurrentlyLoaded /// It is loaded...
				&& /// ...and...
					(InstructionAlreadyInQueueForScene(incomingSetup.sceneAssets[i], LoadAction.Unload) /// ...its either already marked for unloading in the queue...
				|| /// ...or...
					incomingSetup.sceneAssets[i].allowMultipleInstances))) /// ...multiple instances have been manually allowed for this scene.
				{
					loadQueue.Add(incomingSetup.sceneAssets[i], LoadAction.Load); /// If any of those are the case, we can add it to the loadQueue.
				}
				} else
				{
					Warning("There's an empty scene asset slot [{0}] in this setup [{1}]. It will be skipped.", i, incomingSetup.name);
				}
			}

			/// Load / Unload [Incoming]
			yield return RunLoadQueue();

			/// Setup Loaded [Incoming]
			for (int i = 0; i < currentlyLoadedScenes.Count; i++) {
				currentlyLoadedScenes[i].onSetupLoaded.Invoke();

				if (currentlyLoadedScenes[i] != null) {
					if (currentlyLoadedScenes[i].asset.setupLoadedIsOverridden) {
						yield return currentlyLoadedScenes[i].SetupLoaded();
					} else {
						currentlyLoadedScenes[i].SetupLoadedVoid();
					}
				}
			}

			/// Invoke the SetupLoaded event [Incoming]
			onSetupLoaded.Invoke(incomingSetup);

			/// Activate [During Loading Screen]
			if (activeVanillaScene && activeVanillaScene.activationMode == SceneActivationMode.Immediately) {
				SceneManager.SetActiveScene(activeVanillaScene.gameObject.scene);

				activeVanillaScene.onActivation.Invoke();

				if (activeVanillaScene.asset.activateIsOverridden) {
					yield return activeVanillaScene.Activate();
				} else {
					activeVanillaScene.ActivateVoid();
				}

				onSceneActivated.Invoke(activeVanillaScene);
			}

			/// Swap over the setups [Incoming]
			currentSetup = incomingSetup;

			/// End the loading screen [Incoming]
			if (currentLoadingScreen) {
				currentLoadingScreen.gameObject.SetActive(true);

				if (currentLoadingScreen.beginIsOverridden) {
					yield return currentLoadingScreen.End();
				} else {
					currentLoadingScreen.EndVoid();
				}

				currentLoadingScreen.gameObject.SetActive(false);
			}

			/// Activate [After Loading Screen]
			if (activeVanillaScene && activeVanillaScene.activationMode == SceneActivationMode.AfterLoadingScreen) {
				SceneManager.SetActiveScene(activeVanillaScene.gameObject.scene);

				activeVanillaScene.onActivation.Invoke();

				if (activeVanillaScene.asset.activateIsOverridden) {
					yield return activeVanillaScene.Activate();
				} else {
					activeVanillaScene.ActivateVoid();
				}

				onSceneActivated.Invoke(activeVanillaScene);
			}

			setupIsLoading = false;
		}

		public void CancelCurrentLoadProcess() {
			// Woah! This is a very dangerous thing to do! Only call this if you're absolutely certain its necessary.
			if (loadProcess != null) {
				StopCoroutine(loadProcess);
				Warning("The current load process has been cancelled.");
			}
		}

		public void ClearLoadQueue() {
			Log("Clearing the LoadQueue");
			loadQueue.Clear();
		}

		public bool InstructionAlreadyInQueueForScene(VanillaSceneAsset asset, LoadAction actionType) {
			return loadQueue.ContainsKey(asset) && loadQueue[asset] == actionType;
		}


		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Helper Functions
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public void MoveObjectToScene(GameObject objectToMove, Scene scene) {
			SceneManager.MoveGameObjectToScene(objectToMove, scene);
		}

		public void MoveObjectToScene(GameObject objectToMove, VanillaScene vanillaScene) {
			SceneManager.MoveGameObjectToScene(objectToMove, vanillaScene.gameObject.scene);
		}

		public void MoveObjectToScene(GameObject objectToMove, string sceneName) {
			SceneManager.MoveGameObjectToScene(objectToMove, SceneManager.GetSceneByName(sceneName));
		}

		/// <summary>
		/// This will load the scene instantly and unceremoniously; Unity will probably gag for a second unless its very small.
		/// </summary>
		/// <param name="asset">The scene to load.</param>
		public void LoadScene(VanillaSceneAsset asset) {
			Log("Lazy loading [{0}]", asset.name);
			SceneManager.LoadScene(asset.name, LoadSceneMode.Additive);
		}

		public IEnumerator LoadScene(VanillaSceneAsset asset, LoadSceneMode loadMode) {
			AsyncOperation async = SceneManager.LoadSceneAsync(asset.name, loadMode);

			async.allowSceneActivation = false;

			while (async.progress < 0.9f) {
				yield return null;
			}

			//yield return SceneManager.LoadSceneAsync(asset.name, loadMode);

			onSceneLoaded.Invoke(asset.loadedScene);

			//VanillaScene newScene = currentlyLoadedScenes.Find(s => s.sceneAsset == asset);

			if (asset.loadedScene.GetType().GetMethod("SceneLoaded").DeclaringType != typeof(VanillaScene)) {
				Error("YEAAAAAAAAH");
				yield return asset.loadedScene.SceneLoaded();
			} else {
				asset.loadedScene.SceneLoadedVoid();
			}

			if (asset.setupLoadedIsOverridden) {
				yield return asset.loadedScene.SetupLoaded();
			} else {
				asset.loadedScene.SetupLoadedVoid();
			}

			asset.loadedScene.onSetupLoaded.Invoke();
		}

		/// <summary>
		/// This will unload the scene instantly and unceremoniously; Unity will probably gag for a second unless its very small.
		/// </summary>
		/// <param name="asset">The scene to unload.</param>
		public void LazyUnload(VanillaSceneAsset asset) {
			Log("Lazy unloading [{0}]", asset.name);
			SceneManager.UnloadSceneAsync(asset.name);
		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Loading Process Coroutines
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//public IEnumerator ActivateScene() {
		//	if (activeVanillaScene) {
		//		SceneManager.SetActiveScene(activeVanillaScene.gameObject.scene);

		//		activeVanillaScene.onActivation.Invoke();

		//		if (activeVanillaScene.activateIsOverridden) {
		//			yield return activeVanillaScene.Activate();
		//		} else {
		//			activeVanillaScene.ActivateVoid();
		//		}

		//		onSceneActivated.Invoke(activeVanillaScene);
		//	}
		//}

		public IEnumerator RunLoadQueue() {
			foreach (KeyValuePair<VanillaSceneAsset, LoadAction> loadInstruction in loadQueue) {
				Log("Processing LoadQueue entry Scene [{0}] Mode [{1}] BuildID [{2}]", loadInstruction.Key, loadInstruction.Value, loadInstruction.Key.buildID);
				switch (loadInstruction.Value) {
					case LoadAction.Load:
						onNewSceneLoadingDescription.Invoke(loadInstruction.Key.name);

						AsyncOperation async = SceneManager.LoadSceneAsync(loadInstruction.Key.buildID, LoadSceneMode.Additive);

						async.allowSceneActivation = true;

						while (loadInstruction.Key.loadedScene == null) {
							yield return null;
						}

						if (loadInstruction.Key.sceneLoadedIsOverridden) {
							yield return loadInstruction.Key.loadedScene.SceneLoaded();
						} else {
							loadInstruction.Key.loadedScene.SceneLoadedVoid();
						}

						onSceneLoaded.Invoke(loadInstruction.Key.loadedScene);
						break;
					case LoadAction.Unload:
						onSceneUnloaded.Invoke(loadInstruction.Key.loadedScene);

						if (loadInstruction.Key.sceneUnloadIsOverridden) {
							yield return loadInstruction.Key.loadedScene.SceneUnloaded();
						} else {
							loadInstruction.Key.loadedScene.SceneUnloadedVoid();
						}

						yield return SceneManager.UnloadSceneAsync(loadInstruction.Key.loadedScene.gameObject.scene);
						break;
					default:
						Warning("Somehow, an invalid LoadAction [ {0} - {1} ] has been passed into the LoadQueue. Only Load or Unload are technically possible at this point.", loadInstruction.Key, loadInstruction.Value);
						break;
				}
			}

			ClearLoadQueue();
		}
	}
}