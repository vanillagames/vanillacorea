﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla {
	public class VanillaCameras : VanillaSystemModule<VanillaCameras> {

		public Dictionary<string, VanillaCamera> cameras = new Dictionary<string, VanillaCamera>();

		public override IEnumerator Initialize() {
			VanillaCamera[] systemCameras = GetComponentsInChildren<VanillaCamera>(true);

			for (int i = 0; i < systemCameras.Length; i++) {
				systemCameras[i].Initialize();
			}

			//GetCamera()

			yield return null;
		}

		public override void SystemInitializationComplete() {
			VanillaCamera[] systemCameras = GetComponentsInChildren<VanillaCamera>(true);

			for (int i = 0; i < systemCameras.Length; i++) {
				systemCameras[i].PostInitialize();
			}
		}

		public void AddToDictionary(string nickname, VanillaCamera camera) {
			cameras.Add(nickname, camera);
		}

		public void RemoveFromDictionary(string nickname) {
			cameras.Remove(nickname);
		}

		public VanillaCamera GetVanillaCamera(string key) {
			if (cameras.ContainsKey(key)) {
				return cameras[key];
			} else {
				Error("The requested VanillaCamera [{0}] isn't in the dictionary. Make sure its been added first.");
				return null;
			}
		}

		public Camera GetCamera(string key) {
			if (cameras.ContainsKey(key)) {
				return cameras[key].cam;
			} else {
				Error("The requested camera component [{0}] isn't in the dictionary. Make sure its been added first.");
				return null;
			}
		}
	}
}