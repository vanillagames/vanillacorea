﻿namespace Vanilla {
	public class ObjectAnchorAutoPose : VanillaBehaviour {

		public bool poseOnSetupCompletion = true;
		public string anchorKey;

		void OnEnable() {
			VanillaSceneLoader.i.onSetupLoaded.AddListener(MatchAnchorOnSetupSync);
		}

		void OnDisable() {
			if (!VanillaSceneLoader.i) {
				if (Vanilla.applicationQuitBegun) {
					return;
				} else {
					Error("No VanillaSceneLoader instance found");
					return;
				}
			}

			VanillaSceneLoader.i.onSetupLoaded.RemoveListener(MatchAnchorOnSetupSync);
		}

		void MatchAnchorOnSetupSync(SceneSetup newSetup) {
			if (poseOnSetupCompletion) {
				if (!VanillaAnchors.i) {
					return;
				}

				if (VanillaAnchors.i.anchors.ContainsKey(anchorKey)) {
					VanillaAnchors.i.anchors[anchorKey].MatchPose(transform);
				}

				//VanillaSceneLoader.i.sceneDictionary[gameObject.scene.name].anchorDictionary[anchorKey].MatchPose(transform);
			}
		}
	}
}