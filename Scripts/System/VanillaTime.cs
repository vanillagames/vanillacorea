﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections;

namespace Vanilla {
	public class VanillaTime : VanillaSystemModule<VanillaTime> {

		[Header("[ Vanilla Time ]")]
		[ReadOnly]
		public float timeSinceSceneSetupBegan;

		[Header("Pre-built Yield Instructions")]
		[Tooltip("Any floats listed here will be turned into a corresponding WaitForSeconds yield instruction that can be used by coroutines repeatedly throughout the project without any unnecessary memory allocations.")]
		public float[] scaledWaitAmounts;
		public float[] unscaledWaitAmounts;

		[Header("Global Time Tracking")]
		public bool countGlobalTime = false;

		public float secondsRaw, seconds, minutes, hours, days;

		public UnityEvent secondElapsed, minuteElapsed, hourElapsed, dayElapsed;

		public WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
		public WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();

		public WaitForSeconds[] scaledWaits;
		public WaitForSecondsRealtime[] unscaledWaits;

		public override IEnumerator Initialize() {
			timeSinceSceneSetupBegan = 0;

			scaledWaits = new WaitForSeconds[scaledWaitAmounts.Length];

			for (int i = 0; i < scaledWaits.Length; i++) {
				scaledWaits[i] = new WaitForSeconds(scaledWaitAmounts[i]);
			}

			unscaledWaits = new WaitForSecondsRealtime[unscaledWaitAmounts.Length];

			for (int i = 0; i < unscaledWaits.Length; i++) {
				unscaledWaits[i] = new WaitForSecondsRealtime(unscaledWaitAmounts[i]);
			}

			yield break;
		}

		public WaitForSeconds GetScaledWait(int id) {
			if (id > -1 && id < scaledWaits.Length) {
				return scaledWaits[id];
			} else {
				Warning("The requested scaledWait id [{0}] is invalid; it's either lower than 0 or greater than the available number of waits. As a result, we'll give you the nearest available wait.");

				if (scaledWaits.Length > 0) {
					return scaledWaits[Mathf.Clamp(id, 0, scaledWaits.Length - 1)];
				} else {
					return null;
				}
			}
		}

		public WaitForSecondsRealtime GetUnscaledWait(int id) {
			if (id > -1 && id < unscaledWaits.Length) {
				return unscaledWaits[id];
			} else {
				Warning("The requested unscaledWait id [{0}] is invalid; it's either lower than 0 or greater than the available number of waits. As a result, we'll give you the nearest available wait.");

				if (unscaledWaits.Length > 0) {
					return unscaledWaits[Mathf.Clamp(id, 0, unscaledWaits.Length - 1)];
				} else {
					return null;
				}
			}
		}

		public override void Update() {
			base.Update();

			if (countGlobalTime) {
				secondsRaw += Time.deltaTime;

				seconds = seconds % 60;
				minutes = (seconds / 60) % 60;
				hours = (seconds / 3600) % 24;
				days = seconds / 86400;

				if (seconds == 0) {
					Log("Second elapsed");

					secondElapsed.Invoke();
				}

				if (minutes == 0) {
					Log("Minute elapsed");

					minuteElapsed.Invoke();
				}

				if (hours == 0) {
					Log("Hour elapsed");

					hourElapsed.Invoke();
				}

				if (days == 0) {
					Log("Day elapsed");

					dayElapsed.Invoke();
				}
			}
		}
	}
}