﻿using UnityEngine;

using System.Collections;

namespace Vanilla {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(LoadingScreen), true)]
	public class LoadingScreenEditor : VanillaEditor {

		LoadingScreen screen;

		public void OnEnable() {
			screen = (LoadingScreen)target;

			OptimizeOverride();
		}

		/// <summary>
		/// Because the setup loading process contains a lot of potential coroutine calls, it would be helpful to know which steps (if any) could theoretically be skipped.
		/// In fact, thats exactly what we can do by checking if the declaring type of each specific method matches that of the base class (this one!). If it doesn't, it must be overridden.
		/// If we keep track of those bools, the SceneLoader can use that information at run-time to know what processes to bypass. Handy! But is this really just something the compiler might already do for us..?
		/// </summary>
		public void OptimizeOverride() {
			System.Type classType = screen.GetType();

			screen.beginIsOverridden = classType.GetMethod("Begin").DeclaringType != typeof(LoadingScreen);
			screen.endIsOverridden = classType.GetMethod("End").DeclaringType != typeof(LoadingScreen);
		}
	}
#endif

	public class LoadingScreen : VanillaBehaviour {

		[Header("[ Loading Screen ]")]

		[ReadOnly]
		public bool beginIsOverridden;
		[ReadOnly]
		public bool endIsOverridden;

		/// <summary>
		/// This is called by VanillaSceneLoader when the loading screen is first activated and should be used like 'OnEnable'.
		/// However, if this particular method isn't overridden, the standard Begin() method will be called instead.
		/// </summary>
		public virtual IEnumerator Begin() {
			yield return null;
		}

		public virtual void BeginVoid() { }

		/// <summary>
		/// This is called by VanillaSceneLoader when the loading screen is about to be turned off and should be used like 'OnDisable'.
		/// If this particular method isn't overridden, the standard End() method will be called instead.
		/// </summary>
		public virtual IEnumerator End() {
			yield return null;
		}

		public virtual void EndVoid() { }
	}
}