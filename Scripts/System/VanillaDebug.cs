﻿using UnityEngine;
using System.Collections;

namespace Vanilla {
	public class VanillaDebug : VanillaSystemModule<VanillaDebug> {

		public bool useDebugCam;

		public GameObject debugCamPrefab;

		[ReadOnly]
		public GameObject debugCam;

		public override IEnumerator Initialize() {
			if (useDebugCam) {
				debugCam = GameObject.Instantiate(debugCamPrefab, transform.position, transform.rotation, transform);
			}

			yield break;
		}

		public static void SetFlag(DebugFlags objectFlags, DebugFlags flagsToSet) {
			objectFlags |= flagsToSet;
		}

		public static void UnsetFlag(DebugFlags objectFlags, DebugFlags flagsToUnset) {
			objectFlags &= (~flagsToUnset);
		}

		public static bool HasFlag(DebugFlags objectFlags, DebugFlags flagToCheck) {
			return (objectFlags & flagToCheck) == flagToCheck;
		}

		public static void ToggleFlags(DebugFlags objectFlags, DebugFlags toggleFlags) {
			objectFlags ^= toggleFlags;
		}
	}
}