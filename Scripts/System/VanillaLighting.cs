﻿using UnityEngine;

using System.Collections;

namespace Vanilla.Lighting {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(VanillaLighting), true)]
	public class VanillaLightingEditor : VanillaEditor {

		VanillaLighting lighting;

		public void OnEnable() {
			lighting = (VanillaLighting)target;
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (GUILayout.Button("Empty light maps")) {
				lighting.ClearLightMaps();
			}

			if (GUILayout.Button("Use Light Map Set 1")) {
				lighting.ApplyLightMapSet(0);
			}

			if (GUILayout.Button("Use Light Map Set 2")) {
				lighting.ApplyLightMapSet(1);
			}

			 
		}
	}
#endif

	[System.Serializable]
	public class VanillaLighting : VanillaSystemModule<VanillaLighting> {

		public int lightMapDebugIndex;

		public LightmapData dataToYooze;

		//[SerializeField]
		//public LightMapSet[] lightMapSets; 

		public override IEnumerator Initialize() {
			yield break;
		}

		public override void SystemInitializationComplete() {

		}

		public void ClearLightMaps() {
			LightmapSettings.lightmaps = new LightmapData[0];
		}

		public void ApplyLightMapSet(int index) {
			//LightmapSettings.lightmaps = lightMapSets[index].payload;
		}
	}

	// Neither LightmapData type is exposed..?
	//[System.Serializable]
	//public class LightMapSet {
	//	[SerializeField]
	//	public LightmapData[] payload;
	//	[SerializeField]
	//	public LightmapData data;
	//	[SerializeField]
	//	public int blah;
	//}
}
