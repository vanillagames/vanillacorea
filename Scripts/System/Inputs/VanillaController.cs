﻿//using UnityEngine;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//namespace Vanilla.Inputs {
//	[Serializable]
//	public class VanillaController : VanillaBehaviour {

//		[Header("Base Class")]
//		public static Dictionary<string, VanillaController> _controllers;
//		public static Dictionary<string, VanillaController> controllers {
//			get {
//				if (_controllers == null) {
//					VanillaLog.Log("Building VanillaController dictionary for the first time.");

//					_controllers = new Dictionary<string, VanillaController>();

//					VanillaController[] currentControllers = FindObjectsOfType<VanillaController>();

//					foreach (VanillaController c in FindObjectsOfType<VanillaController>()) {
//						VanillaLog.Log("I'm adding myself to the controller dictionary [" + c.name + "]");
//						c.AddSelfToControllerDictionary();
//					}
//				}

//				return _controllers;
//			}
//		}

//		[Tooltip("If true, preferredInputField will be set according to the EstablishPreferredInputField function. Otherwise, it will remain unchanged.")]
//		public bool automaticallySetPreferredInputField;
//		public int preferredInputField;

//		public virtual void AddSelfToControllerDictionary() {
//			if (!controllers.ContainsKey(gameObject.name)) {
//				controllers.Add(gameObject.name, this);
//			} else {
//				Warning("I'm attempting to add myself to the controller dictionary, but my gameObject name is already present as an entry!");
//				return;
//			}
//		}

//		public virtual void RemoveSelfFromControllerDictionary() {
//			if (controllers.ContainsKey(gameObject.name)) {
//				controllers.Remove(gameObject.name);
//			} else {
//				Warning("This controller was enabled but not present in the controller dictionary. Please review how this has occurred!");
//			}
//		}

//		public static VanillaController GetController(string controllerName) {
//			if (controllers.ContainsKey(controllerName)) {
//				return controllers[controllerName];
//			} else if (GameObject.Find(controllerName)) {
//				GameObject.Find(controllerName).GetComponent<VanillaController>().AddSelfToControllerDictionary();

//				return controllers[controllerName];
//			} else {
//				VanillaLog.Error("The controller [{0}] was requested from the dictionary, but it hasn't been added and no GameObject with that name exists currently.", controllerName);
//				return null;
//			}
//		}

//		public virtual void Awake() {
//			if (automaticallySetPreferredInputField) {
//				preferredInputField = EstablishPreferredInputField();
//			}
//		}

//		public virtual void Initialize() {
//		}

//		public virtual int EstablishPreferredInputField() {
//			return -1;
//		}

//		public virtual void OnEnable() {
//			AddSelfToControllerDictionary();
//		}

//		public virtual void OnDisable() {
//			RemoveSelfFromControllerDictionary();
//		}
//	}
//}