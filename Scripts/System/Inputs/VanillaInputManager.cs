﻿//using UnityEngine;
//using UnityEngine.Events;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//namespace Vanilla.Inputs {
//	public enum InputParameterRequirements {
//		None,
//		Bool,
//		Float,
//		Int,
//		String,
//		GameObject,
//	}

//	public class ActionHandler {
//		public ActionHandler(int InputID, int EventID) {
//			inputID = InputID;
//			eventID = EventID;
//		}

//		public int inputID;
//		public int eventID;
//	}



//	public class VanillaInputManager : VanillaBehaviour {
//		public Dictionary<string, InputModule> inputDictionary = new Dictionary<string, InputModule>();

//		public InputModule[] inputModules;

//		public void HandleSubscription(string actionName, UnityAction listener) {
//			ActionHandler newActionHandler = FindActionHandler(actionName);

//			if (newActionHandler == null) {
//				Error("No input seems to be responsible for the given action [{0}]",actionName);
//				return;
//			}

//			//inputModules[newActionHandler.inputID].actionHandlers[newActionHandler.eventID].HandleSubscription(listener);
//		}

//		public void HandleSubscription(string actionName, UnityAction<bool> listener) {
//			bool actionHandlerFound = false;
//			int inputID = -1;

//			for (int i = 0; i < inputModules.Length; i++) {
//				for (int a = 0; a < inputModules[i].actionHandlers.Length; a++) {
//					if (Equals(actionName, inputModules[i].actionHandlers[a])) {
//						actionHandlerFound = true;
//						inputID = i;

//						goto LoopExit;
//					}
//				}
//			}

//			LoopExit:

//			inputModules[inputID].HandleSubscription(listener);
//		}

//		public void HandleSubscription(string actionName, UnityAction<float> listener) {

//		}

//		ActionHandler FindActionHandler(string actionName) {
//			for (int i = 0; i < inputModules.Length; i++) {
//				for (int a = 0; a < inputModules[i].actionHandlers.Length; a++) {
//					if (Equals(actionName, inputModules[i].actionHandlers[a])) {
//						return new ActionHandler(i,a);
//					}
//				}
//			}

//			return null;
//		}
//	}
//}