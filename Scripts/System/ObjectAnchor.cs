﻿using UnityEngine;

using Vanilla.CustomEvents;

namespace Vanilla {
	//[ExecuteInEditMode]
	public class ObjectAnchor : VanillaBehaviour {

		public string anchorName;
		public Mesh customGizoMesh;
		public Color gizmoColor;
		public float gizmoScale;
		public Vector3 meshOffset;
		public Vector3 rotationOffset;

		public void OnEnable() {
			if (!VanillaSceneLoader.i) {
				Error("No VanillaSceneLoader instance currently available.");
				return;
			}

			//VanillaSceneLoader.i.sceneDictionary[gameObject.scene.name].anchorDictionary.Add(anchorName, this);

			if (VanillaAnchors.i == null) {
				return;
			}

			if (VanillaAnchors.i.anchors.ContainsKey(anchorName)) {
				Log("My anchor name [{0}] is already present in the dictionary! I'm going to replace its value (an ObjectAnchor instance) with myself, because there can only be one!");
				VanillaAnchors.i.anchors[anchorName] = this;
				return;
			} else {
				VanillaAnchors.i.anchors.Add(anchorName, this);
			}
		}

		public void OnDisable() {
			if (!VanillaAnchors.i) {
				if (!Vanilla.applicationQuitBegun) {
					Error("Vanilla Anchors could not be found!");
				}

				return;
			}

			VanillaAnchors.i.anchors.Remove(anchorName);
		}

		public void MatchPose(Transform t) {
			t.position = transform.position;
			t.rotation = transform.rotation;
		}

		public void OnDrawGizmos() {
			Gizmos.color = gizmoColor;

			if (customGizoMesh) {
				Gizmos.DrawMesh(customGizoMesh, transform.position + meshOffset, Quaternion.Euler(transform.eulerAngles + rotationOffset), Vector3.one * gizmoScale);
			} else {
				Gizmos.DrawCube(transform.position + (Vector3.up * (gizmoScale * 0.5f)), Vector3.one * gizmoScale);
			}
		}
	}
}