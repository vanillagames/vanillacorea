﻿using UnityEngine;

using System.Collections;

namespace Vanilla {
	/// <summary>
	/// This is the base class of all VanillaSystemModules. Anything included here will be accessible in any VanillaSystemModules.
	/// </summary>
	public class VanillaSystemModuleBase : VanillaBehaviour {
		[Header("[ System Module ]")]

		[Tooltip("If nominated, this system module can elect to be unused. As a result, it won't go through any initialization and will be disabled on startup unless something specifies otherwise.")]
		public bool inUse = true;

		[ReadOnly]
		public bool initialized;

		public virtual void Update() {
			if (!initialized) {
				return;
			}
		}

		public virtual IEnumerator Initialize() {
			Log("Initializing Vanilla System Module [{0}]", GetType().Name);
			yield break;
		}

		public virtual void AssignSingleton() {}

		public virtual void SystemInitializationComplete() { }
	}
}