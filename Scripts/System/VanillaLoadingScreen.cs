﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;

using Vanilla.Math;
using Vanilla.Transitions;

namespace Vanilla {
	public class VanillaLoadingScreen : LoadingScreen {

		[Header("[ Vanilla Loading Screen ]")]

		public TransitionConfiguration inTransition;
		public TransitionConfiguration outTransition;

		public Image logo;
		public Text loadingText;

		Color logoColor;
		float textTimer;

		public float sinWaveSpeed = 2f;
		public float sinWaveOffset = 0.5f;
		public float sinWaveDepth = 0.475f;

		public string[] loadingTexts;

		int textChapter = 0;

		void Awake() {
			logoColor = logo.color;
			logoColor.a = 0.5f;
		}

		public override IEnumerator Begin() {
			while (VanillaTransitions.i.transitioning) {
				Log("Waiting for another transition to finish...");
				yield return null;
			}

			yield return VanillaTransitions.i.Animate(inTransition);

			textTimer = 1.0f;
		}

		public void Update() {
			logoColor.a = sinWaveOffset + (VanillaMath.SinWave(sinWaveSpeed) * sinWaveDepth);
			logo.color = logoColor;

			textTimer -= Time.deltaTime;

			if (textTimer < 0) {
				textTimer = 0.333f;

			if (++textChapter > 3) {
				textChapter = 0;
			}

			loadingText.text = loadingTexts[textChapter];
		}
	}

	public override IEnumerator End() {
			while (VanillaTransitions.i.transitioning) {
				Log("Waiting for another transition to finish...");
				yield return null;
			}

			yield return VanillaTransitions.i.Animate(outTransition);

		}
	}
}