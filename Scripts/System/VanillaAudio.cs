﻿using UnityEngine;
using System.Collections;

using Vanilla;

[System.Serializable]
[RequireComponent(typeof(AudioSource))]
public class VanillaAudio : VanillaSystemModule<VanillaAudio> {

	[HideInInspector]
	public AudioSource speaker;

	public AudioClip[] systemSounds;

	public override IEnumerator Initialize() {
		speaker = GetComponent<AudioSource>();

		yield break;
	}

	public void PlaySystemSound(int id) {
		speaker.PlayOneShot(systemSounds[id]);
	}

	public void PlayClip(AudioClip clip) {
		speaker.PlayOneShot(clip);
	}

	public void PlayClipWithRandomPitchModulation(AudioClip clip, float pitchModulationRange) {
		speaker.pitch = 1.0f + ((Random.value - 0.5f) * pitchModulationRange);
		//speaker.pitch = 0.5f + (Random.value * pitchModulationRange);

		speaker.PlayOneShot(clip);
	}
}