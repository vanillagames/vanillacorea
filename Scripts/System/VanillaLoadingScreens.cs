﻿using UnityEngine;

using System.Collections;

namespace Vanilla {
	public class VanillaLoadingScreens : VanillaSystemModule<VanillaLoadingScreens> {
		[Header("Loading Screens")]

		[HideInInspector]
		public LoadingScreen[] loadingScreens;

		public override IEnumerator Initialize() {
			loadingScreens = GetComponentsInChildren<LoadingScreen>(true);

			for (int i = 0; i < loadingScreens.Length; i++) {
				loadingScreens[i].gameObject.SetActive(false);
			}

			yield return null;
		}

		public LoadingScreen GetLoadingScreenOld(int screenID) {
			if (screenID < 0 || screenID >= loadingScreens.Length) {
				Error("That loading screen index [{0}] is out of range. We'll use the default loading screen for now.", screenID);
				screenID = Mathf.Clamp(screenID, 0, loadingScreens.Length - 1);
			}

			return loadingScreens[screenID];
		}

		public LoadingScreen GetLoadingScreen(int id) {
			if (id == -1) {
				return null;
			}

			if (id < 0 || id >= loadingScreens.Length) {
				Warning("The given loading screen ID [{0}] isn't valid.");

				if (loadingScreens.Length < 0) {
					Warning("There are no loading screens available to use at all, so using a fallback isn't possible either.");
					return null;
				} else {
					id = Mathf.Clamp(id, 0, loadingScreens.Length);
					return loadingScreens[id];
				}
			} else {
				return loadingScreens[id];
			}
		}
	}
}