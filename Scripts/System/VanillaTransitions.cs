﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla.Transitions {
	public class VanillaTransitions : VanillaSystemModule<VanillaTransitions> {

		[ReadOnly]
		public bool transitioning;

		public override IEnumerator Initialize() {
			TransitionBase[] transitions = GetComponentsInChildren<TransitionBase>();

			for (int i = 0; i < transitions.Length; i++) {
				transitions[i].asset.component = transitions[i];
			}

			yield return null;
		}

		public IEnumerator Animate(TransitionConfiguration config) {
			if (transitioning) {
				Log("Waiting for current transition animation to complete before proceeding...");
				yield return null;
			}

			while (transitioning) {
				yield return null;
			}

			if (!config.asset.component) {
				Error("This transition [{0}] hasn't properly assigned its component and thus isn't animateable.", config.asset.name);
				yield break;
			}

			float timeToTake = Mathf.Clamp(config.animationTimer, 0.0f, 100.0f);

			transitioning = true;

			yield return config.asset.component.Animate(timeToTake, config.animationInterpolation);

			transitioning = false;
		}
	}
}