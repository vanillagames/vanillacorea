﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla.VanillaText {
	public static class VanillaText {
		/// <summary>
		/// This will return a string the exact same as the one passed in except with spaces added in behind all initial uppercase letters. Note that it won't add a space to second/third/etc uppercase letters in a row.
		/// </summary>
		/// <param name="input">The string to add spaces to</param>
		/// <returns></returns>
		public static string Spacify(string input) {
			List<char> chars = new List<char>();

			chars.Add('[');

			chars.Add(' ');

			for (int i = 0; i < input.Length; i++) {
				chars.Add(input[i]);

				if (char.IsUpper(input[i]) && (i != 0 && !char.IsUpper(input[i - 1]))) {
					chars.Insert(chars.Count - 1, ' ');
				}
			}

			chars.Add(' ');

			chars.Add(']');

			return new string(chars.ToArray());
		}
		
		/// <summary>
		///		Used for extracting strings from a larger string by looking for an instance of 'snipPhrase' and cutting what follows.
		/// </summary>
		/// 
		/// <param name="input">The body of text to search through</param>
		/// <param name="snipPhrase">The phrase to search for</param>
		/// <param name="expectedLengthOfSnip">How many characters to cut out following snipPhrase. This assumes that we know the length of the desired 'token' string.</param>
		/// 
		/// <returns>
		///		Returns a string containing the contents that follow the first instance of 'snipPhrase'.
		/// </returns>
		/// 
		/// <example>
		///		<code>
		///			SnipOutFirstInstanceOfToken("Cat: 'Tigger' Cat: 'Bella'", "Cat: '", 3) would return [Tig]
		///		</code>
		/// </example>
		public static string SnipOutFirstInstanceOfToken(string input, string snipPhrase, int expectedLengthOfSnip) {
			int snipStart = input.IndexOf(snipPhrase);

			if (snipStart != -1) {
				return input.Substring(snipStart + snipPhrase.Length, expectedLengthOfSnip);
			} else {
				VanillaLog.Error("VanillaText [SnipOutFirstInstanceOfToken]: Could not find any instance of snipPhrase in the input body.");
				return null;
			}
		}

		/// <summary>
		///		Used for extracting strings from a larger string by looking for an instance of 'snipPhrase' and cutting what follows.
		/// </summary>
		/// 
		/// <param name="input">The body of text to search through</param>
		/// <param name="snipPhrase">The phrase to search for</param>
		/// <param name="snipUntilNextOfThisChar">Each snipped string will end at the next instance of this character after snipPhrase. Useful as long as you know which character encapsulates the desired string!</param>
		/// 
		/// <returns>
		///		Returns a string containing the contents that follow the first instance of 'snipPhrase'.
		/// </returns>
		/// 
		/// <example>
		///		<code>
		///			SnipOutFirstInstanceOfToken("Cat: 'Tigger' Cat: 'Bella'", "Cat: '", ''') would return [Tigger]
		///		</code>
		/// </example>
		public static string SnipOutFirstInstanceOfToken(string input, string snipPhrase, char snipUntilNextOfThisChar) {
			int snipStart = input.IndexOf(snipPhrase);

			if (snipStart != -1) {
				input = input.Remove(0, snipStart + snipPhrase.Length);
				return input.Substring(0, input.IndexOf(snipUntilNextOfThisChar));
			} else {
				VanillaLog.Error("VanillaText [SnipOutFirstInstanceOfToken]: Could not find any instance of snipPhrase in the input body.");
				return null;
			}
		}

		/// <summary>
		///		Used for extracting strings from a larger string by looking for instances of 'snipPhrase' and cutting what follows.	
		/// </summary>
		/// 
		/// <param name="input">The body of text to search through</param>
		/// <param name="snipPhrase">The phrase to search for</param>
		/// <param name="expectedLengthOfSnip">How many characters to cut out following snipPhrase. This assumes that we know the length of the desired 'token' string.</param>
		/// 
		/// <returns>
		///		Returns a list of strings containing what follows each instance of 'snipPhrase'
		/// </returns>
		/// 
		/// <example>
		///		<code>
		///			SnipAllInstancesOfToken("Cat: 'Tigger' Cat: 'Bella'", "Cat: '", 3) would return [Tig] and [Bel]
		///		</code>
		/// </example>
		public static List<string> SnipAllInstancesOfToken(string input, string snipPhrase, int expectedLengthOfSnip) {
			int trimPosition = input.IndexOf(snipPhrase);

			if (trimPosition == -1) {
				VanillaLog.Error("VanillaText [SnipAllInstancesOfToken]: Could not find any instance of snipPhrase in the input body.");
				return null;
			}

			string inputTemp = input;

			List<String> output = new List<string>();

			while (trimPosition != -1) {
				inputTemp = inputTemp.Remove(0, trimPosition + snipPhrase.Length);

				output.Add(inputTemp.Substring(0, expectedLengthOfSnip));

				inputTemp.Remove(0, expectedLengthOfSnip);

				trimPosition = inputTemp.IndexOf(snipPhrase);
			}

			return output;
		}

		/// <summary>
		///		Used for extracting strings from a larger string by looking for instances of 'snipPhrase' and cutting what follows.	
		/// </summary>
		/// 
		/// <param name="input">The body of text to search through</param>
		/// <param name="snipPhrase">The phrase to search for</param>
		/// <param name="snipUntilNextOfThisChar">Each snipped string will end at the next instance of this character after snipPhrase. Useful as long as you know which character encapsulates the desired string!</param>
		/// 
		/// <returns>
		///		Returns a list of strings containing what follows each instance of 'snipPhrase'
		/// </returns>
		/// 
		/// <example>
		///		<code>
		///			SnipAllInstancesOfToken("Cat: 'Tigger' Cat: 'Bella'", "Cat: '", ''') would return [Tigger] and [Bella]
		///		</code>
		/// </example>
		public static List<string> SnipAllInstancesOfToken(string input, string snipPhrase, char snipUntilNextOfThisChar) {
			int trimPosition = input.IndexOf(snipPhrase);

			if (trimPosition == -1) {
				VanillaLog.Error("VanillaText [SnipAllInstancesOfToken]: Could not find any instance of snipPhrase in the input body.");
				return null;
			}

			string inputTemp = input;

			List<String> output = new List<string>();

			while (trimPosition != -1) {
				inputTemp = inputTemp.Remove(0, trimPosition + snipPhrase.Length);

				int snipIndex = inputTemp.IndexOf(snipUntilNextOfThisChar);

				output.Add(inputTemp.Substring(0, snipIndex));

				inputTemp.Remove(0, snipIndex);

				trimPosition = inputTemp.IndexOf(snipPhrase);
			}

			return output;
		}
	}
}

