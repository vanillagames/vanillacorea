﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla {
	// Note that this isn't a SystemModule! It has nothing it needs to initializ (currently), so this suits it better...
	public class VanillaAnchors : VanillaSystemModule<VanillaAnchors> {

		public Dictionary<string, ObjectAnchor> anchors = new Dictionary<string, ObjectAnchor>();
	}
}