﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;

namespace Vanilla {
	public static class VanillaLog {

		public static int logMax;

		public static List<string> logs;

		public static Text worldConsole;
#if VanillaDebug
		static List<string> messages = new List<string>();
#endif

		public static void Log(object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.Log(message);

				PrintMessageInWorldConsole("[Log] " + message.ToString());
			}
#endif
		}

		public static void Log(string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.LogFormat(message, parameters);

				PrintMessageInWorldConsole("[Log] " + string.Format(message, parameters));
			}
#endif
		}

		public static void Warning(object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.LogWarning(message);

				PrintMessageInWorldConsole("[Warning] " + message.ToString());
			}
#endif
		}

		public static void Warning(string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.LogWarningFormat(message, parameters);

				PrintMessageInWorldConsole("[Warning] " + string.Format(message, parameters));
			}
#endif
		}

		public static void Error(object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.LogError(message);

				PrintMessageInWorldConsole("[Error] " + message.ToString());
			}
#endif
		}

		public static void Error(string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				Debug.LogErrorFormat(message, parameters);

				PrintMessageInWorldConsole("[Error] " + string.Format(message, parameters));
			}
#endif
		}

		public static void Log(GameObject sender, object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				if (sender != null) {
					Debug.Log(sender.name + ": " + message, sender);

					PrintMessageInWorldConsole("[Log] [" + sender.name + "] " + message.ToString());
				} else {
					Debug.Log("Unknown Sender: " + message);

					PrintMessageInWorldConsole("[Log] [Unknown Sender] " + message.ToString());
				}
			}
#endif
		}

		public static void Log(GameObject sender, string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				if (sender != null) {
					Debug.LogFormat(sender, sender.name + ": " + message, parameters);

					PrintMessageInWorldConsole("[Log] [" + sender.name + "] " + string.Format(message, parameters));
				} else {
					Debug.LogFormat("[Unknown Sender]: " + message, parameters);

					PrintMessageInWorldConsole("[Log] [Unknown Sender] " + string.Format(message, parameters));
				}
			}
#endif
		}

		public static void Warning(GameObject sender, object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				if (sender != null) {
					Debug.LogWarning(sender.name + ": " + message, sender);

					PrintMessageInWorldConsole("[Warning] [" + sender.name + "] " + message.ToString());
				} else {
					Debug.LogWarning("Unknown Sender: " + message);

					PrintMessageInWorldConsole("[Warning] [Unknown Sender] " + message.ToString());
				}
			}
#endif
		}

		public static void Warning(GameObject sender, string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {

				if (sender != null) {
					Debug.LogWarningFormat(sender, sender.name + ": " + message, parameters);

					PrintMessageInWorldConsole("[Warning] [" + sender.name + "] " + string.Format(message, parameters));
				} else {
					Debug.LogWarningFormat("Unknown Sender: " + message, parameters);

					PrintMessageInWorldConsole("[Warning] [Unknown Sender] " + string.Format(message, parameters));
				}
			}
#endif
		}

		public static void Error(GameObject sender, object message) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				if (sender != null) {
					Debug.LogError(sender.name + ": " + message, sender);

					PrintMessageInWorldConsole("[Error] [" + sender.name + "] " + message.ToString());
				} else {
					Debug.LogError("Unknown Sender: " + message);

					PrintMessageInWorldConsole("[Error] [Unknown Sender] " + message.ToString());
				}
			}
#endif
		}

		public static void Error(GameObject sender, string message, params object[] parameters) {
#if VanillaDebug
			if (!Vanilla.applicationQuitBegun) {
				if (sender != null) {
					Debug.LogErrorFormat(sender, sender.name + ": " + message, parameters);

					PrintMessageInWorldConsole("[Error] [" + sender.name + "] " + string.Format(message, parameters));
				} else {
					Debug.LogErrorFormat("Unknown Sender: " + message, parameters);

					PrintMessageInWorldConsole("[Error] [Unknown Sender] " + string.Format(message, parameters));
				}
			}
#endif
		}

		public static void PrintMessageInWorldConsole(string message) {
#if VanillaDebug
			if (worldConsole) {
				messages.Add(message);

				if (messages.Count > logMax) {
					messages.RemoveAt(0);
				}

				Vanilla.i.worldSpaceConsole.text = string.Empty;

				for (int i = 0; i < messages.Count - 1; i++) {
					worldConsole.text += messages[i] + "\n";
				}

				worldConsole.text += messages[messages.Count - 1];
			}
#endif
		}

		public static void ClearWorldSpaceConsole() {
#if VanillaDebug
			if (!worldConsole) {
				Error("The world console can't be cleared because it apparently doesn't exist. Good reason!");
				return;
			}

			messages.Clear();

			worldConsole.text = string.Empty;
#endif
		}
	}
}