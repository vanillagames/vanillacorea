﻿using UnityEngine;
using System.Collections;

namespace Vanilla {
	/// <summary>
	/// This is our dynamic singleton class just for SystemModules, so that each module can identify its own singleton type while sharing
	/// SystemModule stuff through inheritance.
	/// </summary>
	public class VanillaSystemModule<T> : VanillaSystemModuleBase where T : VanillaSystemModuleBase {
		private static T _i;
		public static T i {
			get {
				//if (_i == null) {
					//if (!Application.isPlaying) {
					//return null;
					//}

					//_i = (T)FindObjectOfType(typeof(T));

					//if (_i == null) {
					//if (Vanilla.applicationQuitBegun || !Vanilla.i)
					//VanillaLog.Log("The application is in the process of initializing or quitting, please wait.");
					//}else {
					//VanillaLog.Error("The [{0}] singleton is null, this should never happen. You can start panicking.", typeof(T));
					//}
					//}
				//}

				return _i;
			}
			set {
				_i = value;
			}
		}

		public override void AssignSingleton() {
			i = this as T;
			Log("System singleton online [{0}]", i);
		}
	}
}