﻿using UnityEngine;
using UnityEngine.Events;

using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Text;

using Vanilla.CustomEvents;
using Vanilla.EventSystems;

namespace Vanilla.Inputs {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(VanillaInputs))]
	public class VanillaInputsEditor : VanillaEditor {
		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (GUILayout.Button("Overwrite InputManager with Vanilla InputManager")) {
				string pathToInputManager = Application.dataPath;
				string vanillaInputManager = File.ReadAllText(Application.dataPath + "/+Vanilla/Vanilla/DefaultProjectSettings/InputManager.asset");

				pathToInputManager = pathToInputManager.Substring(0, pathToInputManager.Length - 6) + "ProjectSettings/InputManager.asset";

				File.WriteAllText(pathToInputManager, vanillaInputManager);
			}
		}
	}
#endif

	///-----------------------------------------------------------------------------------------------------------------
	/// VanillaInputs
	/// 
	/// On initialization (and available afterwards as well), VanillaInputs is capable of converting an InputMap asset
	/// file into a live, updating VanillaController component that invokes input-related events. It also handles the 
	/// job of accepting a listener function and ProjectAction enum identifier and figuring out which EventPayloads
	/// are responsible for invoking said function.
	///-----------------------------------------------------------------------------------------------------------------

	public class VanillaInputs : VanillaSystemModule<VanillaInputs> {

		[Header("[ Vanilla Inputs ]")]
		[Tooltip("Which maps should be added automatically from the start of the program?")]
		public InputMap[] initializationMaps;

		[SerializeField]
		public static Dictionary<string, VanillaController> controllers = new Dictionary<string, VanillaController>();

		public override IEnumerator Initialize() {
			yield return base.Initialize();

			for (int i = 0; i < initializationMaps.Length; i++) {
				CreateNewVanillaControllerFromInputMap(initializationMaps[i]);
			}
		}

		public void CreateNewVanillaControllerFromInputMap(InputMap map) {
			GameObject newControllerObject = new GameObject("[VanillaController] " + map.dictionaryName);

			newControllerObject.transform.SetParent(transform);

			VanillaController newController = newControllerObject.AddComponent<VanillaController>();

			newController.Initialize(map);
		}

		public void Subscribe(ProjectAction action, UnityAction callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptSubscription(action, callback);
			}
		}

		public void Subscribe(ProjectAction action, UnityAction<bool> callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptSubscription(action, callback);
			}
		}

		public void Subscribe(ProjectAction action, UnityAction<float> callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptSubscription(action, callback);
			}
		}

		public void Unsubscribe(ProjectAction action, UnityAction callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptUnsubscription(action, callback);
			}
		}

		public void Unsubscribe(ProjectAction action, UnityAction<bool> callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptUnsubscription(action, callback);
			}
		}

		public void Unsubscribe(ProjectAction action, UnityAction<float> callback) {
			for (int i = 0; i < VanillaController.controllers.Count; i++) {
				VanillaController.controllers[i].AttemptUnsubscription(action, callback);
			}
		}
	}

	///-----------------------------------------------------------------------------------------------------------------
	/// VanillaController
	/// 
	/// This class is a run-time representation of a set of inputs, as defined in an InputMap asset file.
	/// When VanillaInputs receives an InputMap, or one is processed during initialization from InitializationMaps,
	/// it will be turned into a VanillaController component on a child object of VanillaInputs.
	/// Any of the input configurations in the InputMap that have at least one 'responsibility' (a ProjectAction)
	/// will be turned into a corresponding InputHandler class.
	/// For this reason, it is best to pre-assign all expected input-project action connections beforehand and then
	/// use VanillaInputs.i.Subscribe or InputHandler.active to control when and how project actions are invoked.
	///-----------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class VanillaController : VanillaBehaviour {

		[Header("[ Vanilla Controller ]")]

		[SerializeField]
		public static List<VanillaController> controllers = new List<VanillaController>();

		[SerializeField]
		public string dictionaryName; // Is this needed? You can simply add the controller to the controller dictionary in the constructor. It might be good for other reasons though, like self removal

		[SerializeField]
		public List<KeyInputHandler> keyInputs;

		[SerializeField]
		public List<ButtonInputHandler> buttonInputs;

		[SerializeField]
		public List<AxisInputHandler> axisInputs;

		[SerializeField]
		public List<MouseAxisInputHandler> mouseAxisInputs;

		public void Initialize(InputMap map) {
			dictionaryName = map.dictionaryName;

			debugFlags = VanillaInputs.i.debugFlags;

			//if (map.keys.Length > 0) {
			keyInputs = new List<KeyInputHandler>();
			//}

			//if (map.buttons.Length > 0) {
			buttonInputs = new List<ButtonInputHandler>();
			//}

			//if (map.axes.Length > 0) {
			axisInputs = new List<AxisInputHandler>();
			//}

			//if (map.mouseAxes.Length > 0) {
			mouseAxisInputs = new List<MouseAxisInputHandler>();
			//}

			for (int i = 0; i < map.keys.Length; i++) {
				if (map.keys[i].HasAtLeastOneResponsibility()) {
					keyInputs.Add(new KeyInputHandler(map.keys[i]));
				} else {
					Log("The key [{0}] has no responsibilities and won't be created in this VanillaController [{1}]", map.keys[i].name, map.dictionaryName);
				}
			}

			for (int i = 0; i < map.buttons.Length; i++) {
				if (map.buttons[i].HasAtLeastOneResponsibility()) {
					buttonInputs.Add(new ButtonInputHandler(map.buttons[i]));
				} else {
					Log("The button [{0}] has no responsibilities and won't be created in this VanillaController [{1}]", map.buttons[i].name, map.dictionaryName);
				}
			}

			for (int i = 0; i < map.axes.Length; i++) {
				if (map.axes[i].HasAtLeastOneResponsibility()) {
					axisInputs.Add(new AxisInputHandler(map.axes[i]));
				} else {
					Log("The axis [{0}] has no responsibilities and won't be created in this VanillaController [{1}]", map.axes[i].name, map.dictionaryName);
				}
			}

			for (int i = 0; i < map.mouseAxes.Length; i++) {
				if (map.mouseAxes[i].HasAtLeastOneResponsibility()) {
					mouseAxisInputs.Add(new MouseAxisInputHandler(map.mouseAxes[i]));
				} else {
					Log("The mouse axis [{0}] has no responsibilities and won't be created in this VanillaController [{1}]", map.mouseAxes[i].name, map.dictionaryName);
				}
			}

			VanillaInputs.controllers.Add(map.dictionaryName, this);

			controllers.Add(this);
		}

		public void Update() {
			for (int i = 0; i < keyInputs.Count; i++) {
				keyInputs[i].GetInputFromUnity();
			}

			for (int i = 0; i < buttonInputs.Count; i++) {
				buttonInputs[i].GetInputFromUnity();
			}

			for (int i = 0; i < axisInputs.Count; i++) {
				axisInputs[i].GetInputFromUnity();
			}

			for (int i = 0; i < mouseAxisInputs.Count; i++) {
				mouseAxisInputs[i].GetInputFromUnity();
			}
		}

		public void AttemptSubscription(ProjectAction action, UnityAction callback) {
			for (int i = 0; i < keyInputs.Count; i++) {
				keyInputs[i].Subscribe(action, callback);
			}

			for (int i = 0; i < buttonInputs.Count; i++) {
				buttonInputs[i].Subscribe(action, callback);
			}

			for (int i = 0; i < axisInputs.Count; i++) {
				axisInputs[i].Subscribe(action, callback);
			}
		}

		public void AttemptSubscription(ProjectAction action, UnityAction<bool> callback) {
			for (int i = 0; i < keyInputs.Count; i++) {
				keyInputs[i].Subscribe(action, callback);
			}

			for (int i = 0; i < buttonInputs.Count; i++) {
				buttonInputs[i].Subscribe(action, callback);
			}
		}

		public void AttemptSubscription(ProjectAction action, UnityAction<float> callback) {
			for (int i = 0; i < axisInputs.Count; i++) {
				axisInputs[i].Subscribe(action, callback);
			}

			for (int i = 0; i < mouseAxisInputs.Count; i++) {
				mouseAxisInputs[i].Subscribe(action, callback);
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction callback) {
			for (int i = 0; i < keyInputs.Count; i++) {
				keyInputs[i].Unsubscribe(action, callback);
			}

			for (int i = 0; i < buttonInputs.Count; i++) {
				buttonInputs[i].Unsubscribe(action, callback);
			}

			for (int i = 0; i < axisInputs.Count; i++) {
				axisInputs[i].Unsubscribe(action, callback);
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction<bool> callback) {
			for (int i = 0; i < keyInputs.Count; i++) {
				keyInputs[i].Unsubscribe(action, callback);
			}

			for (int i = 0; i < buttonInputs.Count; i++) {
				buttonInputs[i].Unsubscribe(action, callback);
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction<float> callback) {
			for (int i = 0; i < axisInputs.Count; i++) {
				axisInputs[i].Unsubscribe(action, callback);
			}

			for (int i = 0; i < mouseAxisInputs.Count; i++) {
				mouseAxisInputs[i].Unsubscribe(action, callback);
			}
		}
	}

	///-----------------------------------------------------------------------------------------------------------------
	/// InputHandlers
	/// 
	/// The role of these classes is to process input from Unity and invoke the corresponding events.
	/// The configuration for these classes, which are created at run-time, come from a pre-existing InputMap asset file.
	///-----------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class InputHandler {

		[SerializeField]
		public string name;

		[SerializeField]
		public bool active = true;

		[SerializeField]
		public bool subscribed = false;

		[SerializeField]
		private int _subscriptionCount;
		public int subscriptionCount {
			get {
				return _subscriptionCount;
			}
			set {
				_subscriptionCount = value <= 0 ? 0 : value;

				if (value > 0) {
					subscribed = true;
					_subscriptionCount = value;
				} else {
					subscribed = false;
					_subscriptionCount = 0;
				}
			}
		}

		public virtual void GetInputFromUnity() { }
	}

	[Serializable]
	public class BoolInputHandler : InputHandler {

		[SerializeField]
		public StandardEventPayload buttonPressed = new StandardEventPayload();

		[SerializeField]
		public StandardEventPayload buttonHeld = new StandardEventPayload();

		[SerializeField]
		public StandardEventPayload buttonReleased = new StandardEventPayload();

		[SerializeField]
		public BoolEventPayload buttonChanged = new BoolEventPayload();

		[SerializeField]
		public BoolEventPayload buttonUpdate = new BoolEventPayload();

		[ReadOnly]
		[SerializeField]
		private bool _currentValue;
		public bool currentValue {
			get {
				return _currentValue;
			}
			set {
				buttonUpdate.payload.Invoke(value);

				switch (state) {
					case ButtonState.Released:
						if (value) {
							state = ButtonState.Pressed;
							buttonChanged.payload.Invoke(value);
						}
						break;
					case ButtonState.Pressed:
						if (value) {
							state = ButtonState.Held;
						} else {
							state = ButtonState.Released;
							buttonChanged.payload.Invoke(value);
						}
						break;
					case ButtonState.Held:
						if (value) {
							state = ButtonState.Held;
						} else {
							state = ButtonState.Released;
							buttonChanged.payload.Invoke(value);
						}
						break;
				}

				_currentValue = value;
			}
		}

		[ReadOnly]
		[SerializeField]
		private ButtonState _state;
		public ButtonState state {
			get {
				return _state;
			}
			set {
				_state = value;

				switch (_state) {
					case ButtonState.Pressed:
						buttonPressed.payload.Invoke();
						break;
					case ButtonState.Held:
						buttonHeld.payload.Invoke();
						break;
					case ButtonState.Released:
						buttonReleased.payload.Invoke();
						break;
				}
			}
		}

		public void Subscribe(ProjectAction action, UnityAction callback) {
			buttonPressed.AttemptSubscription(action, callback, this);
			buttonHeld.AttemptSubscription(action, callback, this);
			buttonReleased.AttemptSubscription(action, callback, this);
		}

		public void Subscribe(ProjectAction action, UnityAction<bool> callback) {
			buttonUpdate.AttemptSubscription(action, callback, this);
			buttonChanged.AttemptSubscription(action, callback, this);
		}

		public void Unsubscribe(ProjectAction action, UnityAction callback) {
			buttonPressed.AttemptUnsubscription(action, callback, this);
			buttonHeld.AttemptUnsubscription(action, callback, this);
			buttonReleased.AttemptUnsubscription(action, callback, this);
		}

		public void Unsubscribe(ProjectAction action, UnityAction<bool> callback) {
			buttonUpdate.AttemptUnsubscription(action, callback, this);
			buttonChanged.AttemptUnsubscription(action, callback, this);
		}
	}

	[Serializable]
	public class KeyInputHandler : BoolInputHandler {

		public KeyInputHandler(InputMap.KeyInputConfig config) {
			name = config.keyCode.ToString();
			keyCode = config.keyCode;

			buttonPressed.actionResponsibilities = config.buttonPressedActions;
			buttonHeld.actionResponsibilities = config.buttonHeldActions;
			buttonReleased.actionResponsibilities = config.buttonReleasedActions;

			buttonChanged.actionResponsibilities = config.buttonChangedActions;

			buttonUpdate.actionResponsibilities = config.buttonUpdateActions;
		}

		[SerializeField]
		public KeyCode keyCode;

		public override void GetInputFromUnity() {
			if (active && subscribed) {
				currentValue = Input.GetKey(keyCode);
			}
		}
	}

	[Serializable]
	public class ButtonInputHandler : BoolInputHandler {

		public ButtonInputHandler(InputMap.ButtonInputConfig config) {
			name = config.name;
			inputString = string.Format("B" + (int)config.unityButtonID);

			buttonPressed.actionResponsibilities = config.buttonPressedActions;
			buttonHeld.actionResponsibilities = config.buttonHeldActions;
			buttonReleased.actionResponsibilities = config.buttonReleasedActions;

			buttonChanged.actionResponsibilities = config.buttonChangedActions;

			buttonUpdate.actionResponsibilities = config.buttonUpdateActions;
		}

		[SerializeField]
		public string inputString;

		public override void GetInputFromUnity() {
			if (active && subscribed) {
				currentValue = Input.GetButton(inputString);
			}
		}
	}

	[Serializable]
	public class FloatInputHandler : InputHandler {
		[SerializeField]
		public string inputString;

		[SerializeField]
		public float changeThreshold;

		/// <summary>
		/// This invokes every frame, regardless.
		/// </summary>
		[SerializeField]
		public FloatEventPayload axisUpdate = new FloatEventPayload();

		/// <summary>
		/// This invokes when the axis value delta breaches a certain amount [changeThreshold]
		/// </summary>
		[SerializeField]
		public FloatEventPayload axisChanged = new FloatEventPayload();
	}

	[Serializable]
	public class AxisInputHandler : FloatInputHandler {
		public AxisInputHandler(InputMap.AxisInputConfig config) {
			name = config.name;

			inputString = string.Format("A" + (int)config.unityAxisID);

			changeThreshold = config.changeThreshold;
			polarityThreshold = config.polarityThreshold;

			axisUpdate.actionResponsibilities = config.axisUpdateActions;

			axisChanged.actionResponsibilities = config.axisChangedActions;

			invert = config.invert;

			axisBecamePositive.actionResponsibilities = config.axisBecamePositiveActions;
			axisBecameNeutral.actionResponsibilities = config.axisBecameNeutralActions;
			axisBecameNegative.actionResponsibilities = config.axisBecameNegativeActions;
		}

		[SerializeField]
		public StandardEventPayload axisBecamePositive = new StandardEventPayload();

		[SerializeField]
		public StandardEventPayload axisBecameNeutral = new StandardEventPayload();

		[SerializeField]
		public StandardEventPayload axisBecameNegative = new StandardEventPayload();

		[SerializeField]
		public float polarityThreshold;

		[SerializeField]
		public bool invert;

		[SerializeField]
		[ReadOnly]
		private float _currentValue;
		public float currentValue {
			get {
				return _currentValue;
			}
			set {
				if (invert) {
					value = -value;
				}

				axisUpdate.payload.Invoke(value);

				// Mathf.Abs returns the value as positive, regardless of whether it was negative/positive to begin with.
				if (Mathf.Abs(value - _currentValue) > changeThreshold) {
					axisChanged.payload.Invoke(value);
				}

				if (value < -polarityThreshold && _currentValue > -polarityThreshold) {
					axisBecameNegative.payload.Invoke();
				}

				if (value > polarityThreshold && _currentValue < polarityThreshold) {
					axisBecamePositive.payload.Invoke();
				}

				if ((value > -polarityThreshold && _currentValue < -polarityThreshold) || (value < polarityThreshold && _currentValue > polarityThreshold)) {
					axisBecameNeutral.payload.Invoke();
				}

				_currentValue = value;
			}
		}

		public override void GetInputFromUnity() {
			if (active && subscribed) {
				currentValue = Input.GetAxisRaw(inputString);
			}
		}

		public void Subscribe(ProjectAction action, UnityAction callback) {
			axisBecamePositive.AttemptSubscription(action, callback, this);
			axisBecameNeutral.AttemptSubscription(action, callback, this);
			axisBecameNegative.AttemptSubscription(action, callback, this);
		}

		public void Subscribe(ProjectAction action, UnityAction<float> callback) {
			axisChanged.AttemptSubscription(action, callback, this);
			axisUpdate.AttemptSubscription(action, callback, this);
		}

		public void Unsubscribe(ProjectAction action, UnityAction callback) {
			axisBecamePositive.AttemptUnsubscription(action, callback, this);
			axisBecameNeutral.AttemptUnsubscription(action, callback, this);
			axisBecameNegative.AttemptUnsubscription(action, callback, this);
		}

		public void Unsubscribe(ProjectAction action, UnityAction<float> callback) {
			axisChanged.AttemptUnsubscription(action, callback, this);
			axisUpdate.AttemptUnsubscription(action, callback, this);
		}
	}

	[Serializable]
	public class MouseAxisInputHandler : InputHandler {
		public MouseAxisInputHandler(InputMap.MouseAxisInputConfig config) {
			name = config.mouseInputType.ToString();

			switch (config.mouseInputType) {
				case MouseAxisType.MouseX:
					inputString = "Mouse X";
					break;
				case MouseAxisType.MouseY:
					inputString = "Mouse Y";
					break;
				case MouseAxisType.ScrollWheel:
					inputString = "Mouse ScrollWheel";
					break;

			}

			changeThreshold = config.changeThreshold;

			axisUpdate.actionResponsibilities = config.axisUpdateActions;
			axisChanged.actionResponsibilities = config.axisChangedActions;
		}

		[SerializeField]
		public string inputString;

		[SerializeField]
		public float changeThreshold;

		[SerializeField]
		public FloatEventPayload axisUpdate = new FloatEventPayload();

		[SerializeField]
		public FloatEventPayload axisChanged = new FloatEventPayload();


		[SerializeField]
		[ReadOnly]
		private float _currentValue;
		public float currentValue {
			get {
				return _currentValue;
			}
			set {
				axisUpdate.payload.Invoke(value);

				// Mathf.Abs returns the value as positive, regardless of whether it was negative/positive to begin with.
				if (Mathf.Abs(value - _currentValue) > changeThreshold) {
					axisChanged.payload.Invoke(value);
				}

				_currentValue = value;
			}
		}

		public override void GetInputFromUnity() {
			if (active && subscribed) {
				currentValue = Input.GetAxisRaw(inputString);
			}
		}

		public void Subscribe(ProjectAction action, UnityAction<float> callback) {
			axisChanged.AttemptSubscription(action, callback, this);
			axisUpdate.AttemptSubscription(action, callback, this);
		}

		public void Unsubscribe(ProjectAction action, UnityAction<float> callback) {
			axisChanged.AttemptUnsubscription(action, callback, this);
			axisUpdate.AttemptUnsubscription(action, callback, this);
		}
	}

	///-----------------------------------------------------------------------------------------------------------------
	/// EventPayload
	/// 
	/// EventPayloads are wrapper classes for UnityEvents that handle which project actions can be subscribed.
	/// They're essentially a gate-keeping class to ensure parameter compatability for subcribed listeners.
	///-----------------------------------------------------------------------------------------------------------------

	[Serializable]
	public class EventPayload {
		[SerializeField]
		public ProjectAction[] actionResponsibilities;
	}

	[Serializable]
	public class StandardEventPayload : EventPayload {
		[SerializeField]
		public UnityEvent payload = new UnityEvent();

		public void AttemptSubscription(ProjectAction action, UnityAction callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.AddListener(callback);
					handler.subscriptionCount++;
				}
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.RemoveListener(callback);
					handler.subscriptionCount--;
				}
			}
		}
	}

	[Serializable]
	public class BoolEventPayload : EventPayload {

		[SerializeField]
		public BoolEvent payload = new BoolEvent();

		public void AttemptSubscription(ProjectAction action, UnityAction<bool> callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.AddListener(callback);
					handler.subscriptionCount++;
				}
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction<bool> callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.RemoveListener(callback);
					handler.subscriptionCount--;
				}
			}
		}
	}

	[Serializable]
	public class FloatEventPayload : EventPayload {

		[SerializeField]
		public FloatEvent payload = new FloatEvent();

		public void AttemptSubscription(ProjectAction action, UnityAction<float> callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.AddListener(callback);
					handler.subscriptionCount++;
				}
			}
		}

		public void AttemptUnsubscription(ProjectAction action, UnityAction<float> callback, InputHandler handler) {
			for (int i = 0; i < actionResponsibilities.Length; i++) {
				if (Equals(actionResponsibilities[i], action)) {
					payload.RemoveListener(callback);
					handler.subscriptionCount--;
				}
			}
		}
	}

	///--------------------------------------------------------------------------------------------------------------------------------------------------------
	/// Input Enums
	///--------------------------------------------------------------------------------------------------------------------------------------------------------

	public enum ButtonState {
		Released,
		Pressed,
		Held
	}

	public enum InputID {
		Zero,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		Eleven,
		Twelve,
		Thirteen,
		Fourteen,
		Fifteen,
		Sixteen,
		Seventeen,
		Eighteen,
		Nineteen,
		Twenty,
		TwentyOne,
		TwentyTwo,
		TwentyThree,
		TwentyFour,
		TwentyFive,
		TwentySix,
		TwentySeven,
		TwentyEight,
		TwentyNine,
		Thirty
	}

	public enum MouseAxisType {
		MouseX,
		MouseY,
		ScrollWheel
	}
}