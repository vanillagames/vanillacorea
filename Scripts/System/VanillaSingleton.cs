﻿using UnityEngine;

using System.Collections;

namespace Vanilla {
	/// <summary>
	/// This is a generic singleton class that can be inherited from. To be honest, I haven't used it a lot yet, I'm just trying it out! So I'll have to
	/// add to this when I know what it can and can't do...
	/// </summary>
	/// <typeparam name="T">The type of class for this singleton.</typeparam>
	public class VanillaSingleton<T> : VanillaBehaviour where T : VanillaBehaviour {
		private static T _i;
		public static T i {
			get {
				if (_i == null) {
					_i = (T)FindObjectOfType(typeof(T));

					if (_i == null) {
						_i.Error("No scripts of this singleton type [{0}] are currently loaded and so we have no singleton either.", typeof(T));
					}
				}

				return _i;
			}
		}
	}
}