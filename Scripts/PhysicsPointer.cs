﻿//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.EventSystems;

//using System.Collections.Generic;

//namespace Vanilla.EventSystems {
//	public class PhysicsPointer : BasePointerOLD {

//		RaycastHit hit;

//		static int worldRayMask;

//		public float rayLength = 50.0f;
//		public bool useLayerMaskOnWorldRay;

//		public void Awake() {
//			if (useLayerMaskOnWorldRay) {
//				worldRayMask = 1 << LayerMask.NameToLayer("UI");
//			}
//		}

//		public override void OnEnable() {
//			base.OnEnable();
//		}

//		public override void OnDisable() {
//			base.OnDisable();
//		}

//		public override void FireRay() {
//			base.FireRay();

//			// Fire a different kind of ray if we want to use a layer mask
//			if (useLayerMaskOnWorldRay) {
//				Physics.Raycast(transform.position, transform.forward, out hit, rayLength, worldRayMask);
//			} else {
//				Physics.Raycast(transform.position, transform.forward, out hit, rayLength);
//			}

//			if (hit.collider) {
//				AssessRayResult(hit.collider.gameObject);
//			}
//		}

//		public override void BuildPointerEventData() {
//			base.BuildPointerEventData();
//		}
//	}
//}