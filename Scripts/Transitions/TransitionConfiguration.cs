﻿using UnityEngine;

using System;

namespace Vanilla.Transitions {
	/// <summary>
	/// This is the class that anything you want to feature a transition should have a copy of.
	/// </summary>
	[Serializable]
	public class TransitionConfiguration {

		[SerializeField]
		public TransitionAsset asset;

		public InterpolationType animationInterpolation;

		[SerializeField]
		public float animationTimer = 1.0f;
	}
}