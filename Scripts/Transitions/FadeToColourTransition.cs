﻿using UnityEngine;

using System.Collections;

using Vanilla.Math;

namespace Vanilla.Transitions {
	public class FadeToColourTransition : TransitionBase {

		public override IEnumerator Animate(float timeInSeconds, InterpolationType interpolationType) {
			Log("Fade-out transition began");

			float rate = 1.0f / timeInSeconds;

			float newNormal = VanillaScreenFader.i.fadeNormal;

			Log("Initial normal is [{0}]", newNormal);
			
			while (newNormal < 1.0f) {
				newNormal += Time.unscaledDeltaTime * rate;

				Log("Normal is now [{0}]", newNormal);

				VanillaScreenFader.i.fadeNormal = VanillaMath.Interpolate(newNormal, interpolationType);

				yield return null;
			}

			Log("Fade-out transition end");
		}
	}
}