﻿using UnityEngine;

namespace Vanilla.Transitions {

	[CreateAssetMenu(fileName ="New Transition Asset", menuName = "Vanilla/Transition Asset")]
	public class TransitionAsset : ScriptableObject {
		//public float defaultAnimationTime = 3.0f;
		//public string nickName;

		//public InterpolationType animationInterpolation;

		[HideInInspector]
		public TransitionBase component;
	}
}