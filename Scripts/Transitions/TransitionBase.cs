﻿using UnityEngine;
using System.Collections;

namespace Vanilla.Transitions {
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(TransitionBase), true)]
	public class TransitionEditor : VanillaEditor {

		TransitionBase transition;

		void OnEnable() {
			transition = (TransitionBase)target;
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (Application.isPlaying) {
				if (GUILayout.Button("Demonstrate Animation")) {
					transition.StartCoroutine(transition.Animate(transition.demonstrationTimer, InterpolationType.Linear));
				}
			}

			 
		}
	}
#endif

	public class TransitionBase : VanillaBehaviour {

		// Nice new pattern that revolves around an asset file which treated like a key/identifier
		// You inherit from TransitionBase as usual for Transition animations, and yield while they complete.
		// But the component stores a reference to the asset file, as well as anywhere that wants to use it.
		// When you want to invoke the animation, you simply pass the asset to VanillaTransitions which handles 
		// running it and waiting.
		// No dictionary whatsoever, also means no string look-up, no out-of-date keys, and easy design integration.
		// ...everyone wins?

		public float demonstrationTimer = 3.0f;

		public TransitionAsset asset;

		public virtual IEnumerator Animate(float timeInSeconds, InterpolationType animationInterpolation) {
			yield break;
		}
	}
}