﻿using UnityEngine;

namespace Vanilla {
	public class VanillaCamera : VanillaBehaviour {

		[Tooltip("This will be used by other scripts to find this camera in the dictionary.")]
		public string cameraNickname = "Vanilla Camera";

		public LayerMask layerMask;

		public Camera _cam;
		public Camera cam {
			get {
				if (_cam == null) {
					FindCamera();

					if (_cam == null) {
						Error("I can't find the camera I'm responsible for controlling!");
					}
				}

				return _cam;
			}
			set {
				_cam = value;
			}
		}

		public virtual void FindCamera() {
			cam = GetComponent<Camera>();
		}

		public virtual void Initialize() {
			Log("Initializing Camera [{0}]", gameObject.name);
			VanillaCameras.i.AddToDictionary(cameraNickname, this);

			cam.cullingMask = layerMask;
		}

		public virtual void PostInitialize() {

		}

		public void OnDestroy() {
			if (VanillaCameras.i != null) {
				VanillaCameras.i.RemoveFromDictionary(cameraNickname);
			}
		}

		public virtual void OnEnable() {
			if (VanillaCameras.i == null) {
				return;
			}
		}

		public virtual void OnDisable() {
			if (VanillaCameras.i == null) {
				return;
			}
		}

		public virtual void ReturnToDefaultSettings() {

		}
	}
}