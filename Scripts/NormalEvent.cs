﻿using UnityEngine;
using UnityEngine.Events;

using System;
using System.Collections;

using Vanilla.CustomEvents;
using Vanilla.Math;

namespace Vanilla {
	[Serializable]
	public class NormalEvent {

		[ReadOnly]
		[SerializeField]
		private float _value;
		public float value {
			get {
				return _value;
			}
			set {
				//VanillaLog.Log("Delta value [{0}] New Value [{1}] New Value Raw [{2}] Difference [{3}]", _value, VanillaMath.Normalize(value + inputOffset, normalMaximum), value, Mathf.Abs(VanillaMath.Normalize(value + inputOffset, normalMaximum) - _value));

				value = flipNormalDirection ? (1 - VanillaMath.Normalize(value + inputOffset, normalMaximum)) : VanillaMath.Normalize(value + inputOffset, normalMaximum);

				if (doTickEvents) {
					if ((Mathf.Abs(value - normalDelta) >= tickIncrement)) {
						normalDelta = VanillaMath.RoundToNearest(value, tickIncrement);

						//VanillaLog.Log("Tick occurred");

						tickEvent.Invoke(value);
					}
				}

				if (invokeEventOnlyIfOutputChanged) {
					if (!Mathf.Approximately(value, _value)) {
						normalEvent.Invoke(value);
					}
				} else {
					normalEvent.Invoke(value);
				}

				_value = value;
			}
		}

		[Tooltip("If only a certain value range needs to be normalized, this value will be added to any newly processed values. For example, if you want to normalize an angle between 30 and 45, this should be set to -30 and maximum set to 15.")]
		[SerializeField]
		public float inputOffset;

		[Tooltip("What maximum value should input be normalized by?")]
		[SerializeField]
		public float normalMaximum = 10.0f;

		[Tooltip("At what output increment should we emit a tick event? Note that this is based on the normalized output value, not the input.")]
		[SerializeField]
		[Range(0,1)]
		public float tickIncrement = 0.1f;

		[Tooltip("If true, the output float event will only invoke if the new value is different to the last frame.")]
		[SerializeField]
		public bool invokeEventOnlyIfOutputChanged;

		[SerializeField]
		public bool doTickEvents;

		public bool flipNormalDirection;

		[ReadOnly]
		public float normalDelta;

		public FloatEvent normalEvent = new FloatEvent();

		public FloatEvent tickEvent = new FloatEvent();

		public void Process(float newValue) {
			value = newValue;
		}
	}
}